package dialogs;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.storjAndroidClient.restClient.StorjRestClient;
import com.storjAndroidClient.restClient.model.BucketEntry;
import com.storjAndroidClient.storjandroid.Config;
import com.storjAndroidClient.storjandroid.R;

import java.io.File;

import activities.InspectBucketActivity;
import environmentCheck.DeviceOnlineChecker;
import implementedListeners.FailedSignatureAuthenticationListener;
import implementedListeners.FileDownloadedListenerImplementation;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class InspectBucketDialogFragment extends DialogFragment {

    InspectBucketActivity inspectBucket;
    StorjRestClient storjRestClient;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        final Resources res = getResources();
        final String[] dialogChoice = { res.getString(R.string.information), res.getString(R.string.delete), res.getString(R.string.download) };

        inspectBucket = ((InspectBucketActivity) getActivity());
        storjRestClient = StorjRestClient.getInstance(Config.URL_BRIDGE_API, getActivity(), new FailedSignatureAuthenticationListener(), inspectBucket.getBaseContext().getSharedPreferences(Config.SHARED_PREF_AUTHENTICATION, MODE_PRIVATE));
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(this.getArguments().getString("file_name"))
                .setItems(dialogChoice, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item

                        if (dialogChoice[which].equals(res.getString(R.string.delete))) {

                            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                            builder1.setMessage(R.string.areYouSure);
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(final DialogInterface dialog, int which) {

                                            // check whether device has internet access and if not, show an alert.
                                            if(!DeviceOnlineChecker.alertIfOffline(inspectBucket.getBaseContext(),
                                                    inspectBucket.getFragmentManager())){
                                                // device is offline, stop the file deleting process
                                                return;
                                            }

                                            //todo delete file
                                        }
                                    });
                            builder1.setNegativeButton(
                                    R.string.cancel,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder1.create();
                            alert.show();

                        }
                        else if (dialogChoice[which].equals(res.getString(R.string.information))) {

                            try {
                                BucketEntry file = storjRestClient.getBucketEntryById(getArguments().getString("file_id"));

                                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                                builder1.setTitle(file.getFilename());
                                builder1.setCancelable(false);
                                builder1.setMessage(
                                        res.getString(R.string.size) + ": " + file.getHumanReadableSize() + "\r\n" +
                                                res.getString(R.string.type) + ": " + file.getMimetype() + "\r\n"
                                );
                                builder1.setPositiveButton(
                                        R.string.back,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });

                                AlertDialog alert = builder1.create();
                                alert.show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else if (dialogChoice[which].equals(res.getString(R.string.download))) {

                            try {

                                // check whether device has internet access and if not, show an alert.
                                if(!DeviceOnlineChecker.alertIfOffline(inspectBucket.getBaseContext(),
                                        inspectBucket.getFragmentManager())){
                                    // device is offline, stop the downloading process
                                    return;
                                }

                                //Displays message to the user
                                String message = getResources().getString(R.string.downloadingFile) + "...";
                                Toast.makeText(getActivity(), message , Toast.LENGTH_LONG).show();

                                //Get Download folder path
                                File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

                                //Get file to be downloaded via file id
                                BucketEntry bucketEntry = storjRestClient.getBucketEntryById(getArguments().getString("file_id"));

                                //Start downloading
                                storjRestClient.downloadFile(bucketEntry, new File(dir + File.separator + bucketEntry.getFilename()), new FileDownloadedListenerImplementation(inspectBucket), inspectBucket);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
        return builder.create();
    }
}

