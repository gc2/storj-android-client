package dialogs;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by Gabriel Comte on 02.12.2016.
 *
 * A simple {@link Fragment} subclass.
 */


public class DeviceOfflineDialogFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setTitle("Device Offline")
                // ab Android 6 Marshmallow
//                .setMessage("Please connect your " + getDeviceTypeAsString(this.getContext()) + " to the Internet to use the Storj App.")
                .setMessage("Please connect your " + getDeviceTypeAsStringVersionHack() + " to the Internet to use the Storj App.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    //todo is only used for backwards compatibility. Not used anymore when supporting only Marshmallow and newer versions
    public String getDeviceTypeAsStringVersionHack(){
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow or newer
                return getDeviceTypeAsString(this.getContext());
            } else {
                return "phone";
            }

    }

    public static String getDeviceTypeAsString(Context context){
        if(isTablet(context)){
            return "tablet";
        }else{
            return "phone";
        }
    }


    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
}

