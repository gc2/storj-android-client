package dialogs;


import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import com.android.volley.Response;
import com.storjAndroidClient.restClient.StorjRestClient;
import com.storjAndroidClient.restClient.model.Bucket;
import com.storjAndroidClient.restClient.model.PublicBucketPermissions;
import com.storjAndroidClient.storjandroid.Config;
import com.storjAndroidClient.storjandroid.R;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import activities.BucketsActivity;
import environmentCheck.DeviceOnlineChecker;
import implementedListeners.FailedSignatureAuthenticationListener;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */


public class BucketDialogFragment extends DialogFragment {

    BucketsActivity bucketsActivity;
    StorjRestClient storjRestClient;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        final Resources res = getResources();
        String[] options = null;
        System.out.println("bucket is public: " + this.getArguments().getBoolean("bucket_is_public"));
        if(this.getArguments().getBoolean("bucket_is_public")){
            options = new String[]{res.getString(R.string.information), res.getString(R.string.delete), res.getString(R.string.changePublicPermissions)};
        }else{
            options = new String[]{ res.getString(R.string.information), res.getString(R.string.delete), res.getString(R.string.makePublic)};
        }
        final String[] dialogChoice = options;

this.getArguments().getBoolean("bucket_has_push");

        bucketsActivity = ((BucketsActivity)getActivity());
        storjRestClient = StorjRestClient.getInstance(Config.URL_BRIDGE_API, getActivity(), new FailedSignatureAuthenticationListener(), bucketsActivity.getBaseContext().getSharedPreferences(Config.SHARED_PREF_AUTHENTICATION, MODE_PRIVATE));
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(this.getArguments().getString("bucket_name"))
                .setItems(dialogChoice, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        if (dialogChoice[which].equals(res.getString(R.string.delete))) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                            builder1.setMessage(R.string.areYouSure);
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(final DialogInterface dialog, int which) {

                                            // check whether device has internet access and if not, show an alert.
                                            if(!DeviceOnlineChecker.alertIfOffline(bucketsActivity.getBaseContext(),
                                                    bucketsActivity.getFragmentManager())){
                                                // device is offline, stop the bucket deleting process
                                                return;
                                            }

                                            storjRestClient.deleteBucket(getArguments().getString("bucket_id"), bucketsActivity.getBaseContext().getSharedPreferences(Config.SHARED_PREF_AUTHENTICATION, MODE_PRIVATE), new Response.Listener<String>() {
                                                //      storjRestClient.deleteBucket(newBucket.getId(), new RequestResponseListener() {  <----- This is also possible!
                                                @Override
                                                public void onResponse(String response) {
                                                    bucketsActivity.refreshBucketsList();
                                                    dialog.dismiss();

                                                }
                                            }, new FailedSignatureAuthenticationListener());
                                        }
                                    });
                            builder1.setNegativeButton(
                                    R.string.cancel,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder1.create();
                            alert.show();

                        }
                        else if (dialogChoice[which].equals(res.getString(R.string.information))) {

                            try {
                                Bucket b = storjRestClient.getBucketById(getArguments().getString("bucket_id"));

                                // change time to users time [localDateTime] and to its format
                                DateTimeFormatter fmt = DateTimeFormat.mediumDateTime();
                                String formattedDateTime = fmt.print(b.getCreatedAsDateTime().toLocalDateTime());

                                String pubBucketPermissions = "";
                                if(b.getPublicPermissions() != null && !b.getPublicPermissions().isEmpty()){
                                    pubBucketPermissions = res.getString(R.string.pubBucketPermissions) + ": " + android.text.TextUtils.join(", ", b.getPublicPermissions()) + "\r\n";
                                }

                                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                                builder1.setTitle(b.getName());
                                builder1.setCancelable(false);
                                builder1.setMessage(
                                        res.getString(R.string.user) + ": " + b.getUser() + "\r\n" +

                                                // as of 2. March 2017, these attributes are not available from the bridge.
//                                                res.getString(R.string.status) + ": " + (b.getStatus().equals("Active") ? res.getString(R.string.active) : res.getString(R.string.inactive)) + "\r\n" +
//                                                res.getString(R.string.storage) + ": " + b.getStorage() + " GB" +  "\r\n" +
//                                                res.getString(R.string.transfer) + ": " + b.getTransfer() + " GB" + "\r\n" +

                                                pubBucketPermissions +
                                                res.getString(R.string.created) + ": " + formattedDateTime
                                );
                                builder1.setPositiveButton(
                                        R.string.back,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });

                                AlertDialog alert = builder1.create();
                                alert.show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else if (dialogChoice[which].equals(res.getString(R.string.makePublic))) {

                            try {
                                final Bucket b = storjRestClient.getBucketById(getArguments().getString("bucket_id"));

                                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                                builder1.setTitle(res.getString(R.string.makePublic) + ": " + b.getName());
                                builder1.setCancelable(true);
                                builder1.setMessage(res.getString(R.string.cautionPublicIrreversible));

                                final Activity activity = getActivity();

                                builder1.setPositiveButton(
                                        R.string.buttonMakePublic,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                dialog.dismiss();

                                                AlertDialog.Builder setPermissions = new AlertDialog.Builder(activity);
                                                setPermissions.setTitle(res.getString(R.string.makePublic) + ": " + b.getName());
                                                setPermissions.setCancelable(true);

                                                final CharSequence[] permissions = {res.getString(R.string.publicPermissionsRead), res.getString(R.string.publicPermissionsWrite), res.getString(R.string.publicPermissionsReadAndWrite)};
                                                setPermissions.setSingleChoiceItems(permissions, -1, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(final DialogInterface radioButtonDialog, int which) {

                                                        // check whether device has internet access and if not, show an alert.
                                                        if(!DeviceOnlineChecker.alertIfOffline(bucketsActivity.getBaseContext(),
                                                                bucketsActivity.getFragmentManager())){
                                                            // device is offline, stop the bucket deleting process
                                                            return;
                                                        }

                                                        List<PublicBucketPermissions> permissionsList = new ArrayList<PublicBucketPermissions>();
                                                        if(permissions[which]==res.getString(R.string.publicPermissionsRead)){
                                                            permissionsList.add(PublicBucketPermissions.PULL);
                                                        }else if(permissions[which]==res.getString(R.string.publicPermissionsWrite)){
                                                            permissionsList.add(PublicBucketPermissions.PUSH);
                                                        }else if(permissions[which]==res.getString(R.string.publicPermissionsReadAndWrite)){
                                                            permissionsList.add(PublicBucketPermissions.PULL);
                                                            permissionsList.add(PublicBucketPermissions.PUSH);
                                                        }

                                                        try {
                                                            storjRestClient.makeBucketPublic(getArguments().getString("bucket_id"), permissionsList, new Response.Listener<JSONObject>() {
                                                                @Override
                                                                public void onResponse(JSONObject response) {
                                                                    bucketsActivity.refreshBucketsList();
                                                                    radioButtonDialog.dismiss();
                                                                }
                                                            }, new FailedSignatureAuthenticationListener());
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                });

                                                setPermissions.create();
                                                setPermissions.show();
                                            }
                                        });

                                AlertDialog alert = builder1.create();
                                alert.show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else if (dialogChoice[which].equals(res.getString(R.string.changePublicPermissions))) {

                            try {
                                final Bucket b = storjRestClient.getBucketById(getArguments().getString("bucket_id"));

                                AlertDialog.Builder radioButtonDialog = new AlertDialog.Builder(getActivity());
                                radioButtonDialog.setTitle(res.getString(R.string.changePublicPermissions) + ": " + b.getName());
                                radioButtonDialog.setCancelable(true);

                                final CharSequence[] permissions = {res.getString(R.string.publicPermissionsRead), res.getString(R.string.publicPermissionsWrite), res.getString(R.string.publicPermissionsReadAndWrite)};

                                radioButtonDialog.setSingleChoiceItems(permissions, -1, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(final DialogInterface radioButtonDialog, int which) {

                                                // check whether device has internet access and if not, show an alert.
                                                if(!DeviceOnlineChecker.alertIfOffline(bucketsActivity.getBaseContext(),
                                                        bucketsActivity.getFragmentManager())){
                                                    // device is offline, stop the bucket deleting process
                                                    return;
                                                }

                                                List<PublicBucketPermissions> permissionsList = new ArrayList<PublicBucketPermissions>();
                                                if(permissions[which]==res.getString(R.string.publicPermissionsRead)){
                                                    permissionsList.add(PublicBucketPermissions.PULL);
                                                }else if(permissions[which]==res.getString(R.string.publicPermissionsWrite)){
                                                    permissionsList.add(PublicBucketPermissions.PUSH);
                                                }else if(permissions[which]==res.getString(R.string.publicPermissionsReadAndWrite)){
                                                    permissionsList.add(PublicBucketPermissions.PULL);
                                                    permissionsList.add(PublicBucketPermissions.PUSH);
                                                }

                                                try {
                                                    storjRestClient.changePublicBucketPermissions(getArguments().getString("bucket_id"), permissionsList, new Response.Listener<JSONObject>() {
                                                        @Override
                                                        public void onResponse(JSONObject response) {
                                                            bucketsActivity.refreshBucketsList();
                                                            radioButtonDialog.dismiss();
                                                        }
                                                    }, new FailedSignatureAuthenticationListener());
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        });

                                AlertDialog alert = radioButtonDialog.create();
                                alert.show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
        return builder.create();
    }

}

