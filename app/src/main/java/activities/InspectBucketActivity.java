package activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Response;
import com.storjAndroidClient.restClient.StorjRestClient;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.AllFilesTransferredListener;
import com.storjAndroidClient.restClient.model.BucketEntry;
import com.storjAndroidClient.storjandroid.Config;
import com.storjAndroidClient.storjandroid.R;

import java.io.File;
import java.util.List;

import adapters.EntryArrayAdapter;
import dialogs.InspectBucketDialogFragment;
import environmentCheck.DeviceOnlineChecker;
import fileBrowser.FileBrowser;
import implementedListeners.FailedSignatureAuthenticationListener;
import implementedListeners.FileUploadedListenerImplementation;

public class InspectBucketActivity extends BaseActivity implements AllFilesTransferredListener {

    private static final int REQUEST_PATH = 1;
    StorjRestClient storjRestClient;
    List<BucketEntry> filesOfCurrentBucket;
    ListView filesListView;
    ProgressBar progressBar;
    String curFileName;
    String bucket_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle b = getIntent().getExtras();
        super.setActionBarTitle(b.getString("bucket_name"));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspect_bucket);
        super.onCreateDrawer(savedInstanceState);

        //Get client instance
        storjRestClient = StorjRestClient.getInstance(Config.URL_BRIDGE_API, this.getApplicationContext(), new FailedSignatureAuthenticationListener(), this.getBaseContext().getSharedPreferences(Config.SHARED_PREF_AUTHENTICATION, MODE_PRIVATE));

        setTitle(b.getString("bucket_name"));
        bucket_id = b.getString("bucket_id");

        progressBar = (ProgressBar) this.findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.GONE);

        refreshBucketList();

    }

    @Override
    protected void onStart() {
        super.onStart();

        // check whether device has internet access and if not, show an alert.
        if(!DeviceOnlineChecker.alertIfOffline(this.getBaseContext(), this.getFragmentManager())){
            // device is offline, stop the filelist loading process
            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.upload_file,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.upload_file:
                getFile(this.findViewById(android.R.id.content));
        }

        return super.onOptionsItemSelected(item);
    }

    //This is called from onOptionsItemSelected when Upload File is clicked
    public void getFile(View view){
        Intent intent1 = new Intent(this, FileBrowser.class);
        startActivityForResult(intent1, REQUEST_PATH);
    }

    // Listen for results.
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        // See which child activity is calling us back.
        if (requestCode == REQUEST_PATH){
            if (resultCode == RESULT_OK) {

                // check whether device has internet access and if not, show an alert.
                if(!DeviceOnlineChecker.alertIfOffline(this.getBaseContext(), this.getFragmentManager())){
                    // device is offline, stop the file uploading process
                    return;
                }

                //Get entry name from extras
                curFileName = data.getStringExtra("GetFileName");

                //Get current path location from extras and combine it with entry name to get final path
                String path = data.getStringExtra("GetPath") + File.separator + curFileName;

                Log.d("Debugging", path);
                try {

                    //Displays message to the user
                    String message = getResources().getString(R.string.uploadingFile) + "...";
                    Toast.makeText(InspectBucketActivity.this, message , Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(ProgressBar.VISIBLE);

                    //Create File path from previous String path
                    final File file = new File(path);

                    //Start uploading file
                    // the interface FileTransferListener and AllFilesTransferredListener can not be transmit as anonymous classes, due to serialization!
                    storjRestClient.uploadFile(file, bucket_id, new FileUploadedListenerImplementation(this), this);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void refreshBucketList(){

        storjRestClient.loadFileListByBucketId(bucket_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    filesOfCurrentBucket = storjRestClient.getFilesOfCurrentFileList();
                    Log.d("FilesList", filesOfCurrentBucket.toString());

                    //Get ListView object from xml
                    filesListView = (ListView) findViewById(R.id.list);
                    // Define a new Adapter
                    if (filesOfCurrentBucket.size() > 0) {
                        filesListView.setAdapter(new EntryArrayAdapter(InspectBucketActivity.this, filesOfCurrentBucket));

                        filesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                InspectBucketDialogFragment dialog = new InspectBucketDialogFragment();
                                Bundle b = new Bundle();
                                b.putString("file_id", filesOfCurrentBucket.get(position).getId());
                                b.putString("file_name", filesOfCurrentBucket.get(position).getName());
                                dialog.setArguments(b);
                                dialog.show(InspectBucketActivity.this.getFragmentManager(), "");

                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onFileTransferOver() {

        System.out.println("================= ON FILES TRANSFERRED! --> nothing is being uploaded or downloaded anymore");

        // todo hier sollte alles hochgeladen sein, aber auch alles heruntergeladen..?? muss gecheckt werden

        // UI tasks must be run on the main thread ==> runOnUiThread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                System.out.println("in runnable");
                System.out.println("is uploading(): " + storjRestClient.isTransferring());

                synchronized (this) {
                    if (!storjRestClient.isTransferring()) {
                        progressBar.setVisibility(ProgressBar.GONE);
                    }
                }
            }
        });

    }
}


