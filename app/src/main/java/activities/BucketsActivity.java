package activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.Response;
import com.storjAndroidClient.restClient.StorjRestClient;
import com.storjAndroidClient.restClient.model.Bucket;
import com.storjAndroidClient.restClient.model.PublicBucketPermissions;
import com.storjAndroidClient.storjandroid.Config;
import com.storjAndroidClient.storjandroid.R;

import org.json.JSONObject;

import java.util.List;

import adapters.BucketArrayAdapter;
import dialogs.BucketDialogFragment;
import environmentCheck.DeviceOnlineChecker;
import implementedListeners.FailedSignatureAuthenticationListener;

public class BucketsActivity extends BaseActivity {

    StorjRestClient storjRestClient;
    List<Bucket> buckets;
    ListView bucketsListView;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setActionBarTitle(getResources().getString(R.string.yourBuckets));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buckets);
        super.onCreateDrawer(savedInstanceState);

        setTitle(getResources().getString(R.string.yourBuckets));

        //Sets up 'New Bucket' button
        SetUpNewBucketButton();

        //Get Instance of storjrestclient
        storjRestClient = StorjRestClient.getInstance(Config.URL_BRIDGE_API, this.getApplicationContext(), new FailedSignatureAuthenticationListener(), this.getBaseContext().getSharedPreferences(Config.SHARED_PREF_AUTHENTICATION, MODE_PRIVATE));

        refreshBucketsList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.create_bucket:
                alertDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_bucket,menu);
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("message", "This is my message to be reloaded");
        super.onSaveInstanceState(outState);
    }

    public void refreshBucketsList() {

        // check whether device has internet access and if not, show an alert.
        if(!DeviceOnlineChecker.alertIfOffline(this.getApplicationContext(), BucketsActivity.this.getFragmentManager())){
            // device is offline, stop the refreshing process
            return;
        }

        storjRestClient.loadBucketList(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //Get all buckets
                try {
                    buckets = storjRestClient.getAllBuckets();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //Get ListView object from xml
                bucketsListView = (ListView) findViewById(R.id.list);
                //Define a new Adapter
                bucketsListView.setAdapter(new BucketArrayAdapter(BucketsActivity.this, buckets));
                //Add OnClick Listeners to every item on the bucketsListView
                bucketsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        // check whether device has internet access and if not, show an alert.
                        if(!DeviceOnlineChecker.alertIfOffline(getBaseContext(), BucketsActivity.this.getFragmentManager())){
                            // device is offline, stop the bucket loading process
                            return;
                        }

                        Intent intent = new Intent(BucketsActivity.this, InspectBucketActivity.class);
                        intent.putExtra("bucket_name", buckets.get(position).getName());
                        intent.putExtra("bucket_id", buckets.get(position).getId());
                        startActivity(intent);
                    }
                });

                bucketsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                        BucketDialogFragment dialog = new BucketDialogFragment();
                        Bundle b = new Bundle();
                        b.putString("bucket_id", buckets.get(position).getId());
                        b.putString("bucket_name", buckets.get(position).getName());
                        System.out.println();
                        if(buckets.get(position).getEncryptionKey() != null && !buckets.get(position).getEncryptionKey().equals("")) {
                            b.putBoolean("bucket_is_public", true);

                            if(buckets.get(position).getPublicPermissionsAsEnums().contains(PublicBucketPermissions.PULL)){
                                b.putBoolean("bucket_has_pull", true);
                            }else {
                                b.putBoolean("bucket_has_pull", false);
                            }

                            if(buckets.get(position).getPublicPermissionsAsEnums().contains(PublicBucketPermissions.PUSH)){
                                b.putBoolean("bucket_has_push", true);
                            }else {
                                b.putBoolean("bucket_has_push", false);
                            }
                        }else{
                            b.putBoolean("bucket_is_public", false);
                        }
                        dialog.setArguments(b);
                        dialog.show(BucketsActivity.this.getFragmentManager(), "");

                        return true;
                    }
                });
                Log.d("Debugging", "Refreshed Buckets!");
            }
        });
    }

    public void SetUpNewBucketButton() {

        //Create Alert button
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        //User input text
        final EditText et = new EditText(this);
        et.setSingleLine(true);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setMessage(R.string.enterBucketName);
        alertDialogBuilder.setView(et);

        //Set dialog message
        alertDialogBuilder.setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                // check whether device has internet access and if not, show an alert.
                if(!DeviceOnlineChecker.alertIfOffline(getBaseContext(), BucketsActivity.this.getFragmentManager())){
                    // device is offline, stop the bucket creating process
                    return;
                }

                // create bucket
                final Bucket newBucket = new Bucket();
                newBucket.setName(et.getText().toString());

                // GCOM: by 2. March 2017, the following settings were completely ignored by the bridge. The only
//                newBucket.setStatus("Active");
//                newBucket.setStorage(10);
//                newBucket.setTransfer(10);
//                newBucket.setUser("joe.doe@acme.com");

                storjRestClient.createBucket(newBucket, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response){

                        //todo this is only for testing purposes
                        // every uploaded bucket should be renamed to xyz. this does however not work (as of 05. March 2017).
                        // it does not work on the CLI either.
                        // The feature of adapting existing buckets does not work in the storj CLI either.
//
//                        Gson gson = new Gson();
//                        Bucket b = gson.fromJson(String.valueOf(response), Bucket.class);
//                        System.out.println(response);
//
//                        b.setName("xyz");
//
//                        storjRestClient.updateBucket(b, getBaseContext().getSharedPreferences(Config.SHARED_PREF_AUTHENTICATION, Context.MODE_PRIVATE), new Response.Listener<JSONObject>() {
//                            @Override
//                            public void onResponse(JSONObject responseObject){
//                                // do nothing
//                            }}, new FailedSignatureAuthenticationListener());

                        refreshBucketsList();

                    }});
            }
        });

        //Create alert dialog
        alertDialog = alertDialogBuilder.create();
    }
}
