package activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.storjAndroidClient.storjandroid.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PreferencesActivity extends BaseActivity {

    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setActionBarTitle(getResources().getString(R.string.preferences));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);
        super.onCreateDrawer(savedInstanceState);

        final String[] listViewItems = { getResources().getString(R.string.small), getResources().getString(R.string.medium), getResources().getString(R.string.big) };
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        edit = prefs.edit();

        listView = (ListView)findViewById(R.id.listView);

        List<Map<String, String>> data = new ArrayList<Map<String, String>>();
        data.add(createPreferenceMenuItem(getResources().getString(R.string.bucketSize), getStringFromSize(prefs.getInt("bucketTextSize", 60))));
        data.add(createPreferenceMenuItem(getResources().getString(R.string.entrySize), getStringFromSize(prefs.getInt("entryTextSize", 60))));

        SimpleAdapter adapter = new SimpleAdapter(this, data,
                android.R.layout.simple_list_item_2,
                new String[] {"title", "subtitle"},
                new int[] {android.R.id.text1,
                        android.R.id.text2});
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                AlertDialog.Builder builder = new AlertDialog.Builder(PreferencesActivity.this);
                builder.setTitle(getResources().getString(R.string.size))
                        .setItems(listViewItems, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // The 'which' argument contains the index position
                                // of the selected item
                                if (position == 0) {
                                    int size = 60;
                                    switch (which) {
                                        case 0:
                                            size = 40;
                                            break;
                                        case 1:
                                            size = 60;
                                            break;
                                        case 2:
                                            size = 80;
                                            break;
                                    }
                                    edit.putInt("bucketTextSize", size);
                                }
                                else {
                                    int size = 60;
                                    switch (which) {
                                        case 0:
                                            size = 40;
                                            break;
                                        case 1:
                                            size = 60;
                                            break;
                                        case 2:
                                            size = 80;
                                            break;
                                    }
                                    edit.putInt("entryTextSize", size);
                                }
                                edit.commit();
                                RefreshPreferenceMenuList();
                            }
                        });
                builder.create();
                builder.show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        RefreshPreferenceMenuList();
    }

    protected void RefreshPreferenceMenuList() {

        List<Map<String, String>> data = new ArrayList<Map<String, String>>();
        data.add(createPreferenceMenuItem(getResources().getString(R.string.bucketSize), getStringFromSize(prefs.getInt("bucketTextSize", 60))));
        data.add(createPreferenceMenuItem(getResources().getString(R.string.entrySize), getStringFromSize(prefs.getInt("entryTextSize", 60))));

        SimpleAdapter adapter = new SimpleAdapter(this, data,
                android.R.layout.simple_list_item_2,
                new String[] {"title", "subtitle"},
                new int[] {android.R.id.text1,
                        android.R.id.text2});
        listView.setAdapter(adapter);
    }

    protected String getStringFromSize(int i) {

        String size = "";
        switch (i) {
            case 40:
                size = getResources().getString(R.string.small);
                break;
            case 60:
                size = getResources().getString(R.string.medium);
                break;
            case 80:
                size = getResources().getString(R.string.big);
                break;

        }
        return size;

    }

    protected Map<String, String> createPreferenceMenuItem(String title, String subtitle) {
        Map<String, String> map = new HashMap<String, String>(2);
        map.put("title", title);
        map.put("subtitle", subtitle);
        return map;

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

}
