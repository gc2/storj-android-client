package activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.storjAndroidClient.restClient.StorjRestClient;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;
import com.storjAndroidClient.restClient.fileHandling.encryption.SeedUtils;
import com.storjAndroidClient.storjandroid.Config;
import com.storjAndroidClient.storjandroid.R;

import org.bitcoinj.crypto.MnemonicException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import environmentCheck.DeviceOnlineChecker;
import implementedListeners.FailedSignatureAuthenticationListener;

public class InitializationActivity extends AppCompatActivity {

    EditText etUsername, etPassword;
    List<EditText> wordTextfields;
    Button loginButton, newAccountButton, importSeedButton;
    boolean seedChecksumJustFailed;
    TextView seedNotValid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        // check whether signature based authentication has already been initialized
        CryptoUtils cryptoUtils = new CryptoUtils(getBaseContext(), getBaseContext().getSharedPreferences(Config.SHARED_PREF_AUTHENTICATION, Context.MODE_PRIVATE));
        if(cryptoUtils.loadPublicKey() != null && !cryptoUtils.loadPublicKey().equals("")){

            // check whether mnemonic has already been imported to the app and seed is available
            SeedUtils seedUtils = new SeedUtils(getBaseContext());
            if(!seedUtils.seedInformationIsStored()){
                this.importSeed();
                return;
            }

            //  signature-based authentication is already installed plus encryption seed is installed - start BucketsActivity
            Intent intent = new Intent(this, BucketsActivity.class);
            startActivity(intent);
            return;
        }

        // DISPLAY LOGIN
        setContentView(R.layout.activity_initialization);

        //Get and set storj logo
        ImageView storjLogoImage = (ImageView) findViewById(R.id.storjLogo);
        storjLogoImage.setImageResource(R.drawable.storj_logo);

        etUsername = (EditText) findViewById(R.id.loginText);
        etPassword = (EditText) findViewById(R.id.passwordText);

        loginButton = (Button) findViewById(R.id.login_button);
        newAccountButton = (Button) findViewById(R.id.new_account_button);
    }

    public void OnLoginButtonClick(View view) {

        // check whether device has internet access and if not, show an alert.
        if (!DeviceOnlineChecker.alertIfOffline(this.getApplicationContext(), InitializationActivity.this.getFragmentManager())) {
            // device is offline, stop the login process
            return;
        }

        // disable buttons
        loginButton.setEnabled(false);
        loginButton.setBackgroundColor(Color.GRAY);
        newAccountButton.setEnabled(false);
        newAccountButton.setBackgroundColor(Color.GRAY);

        final Context context = getBaseContext();

            loginUser(etUsername.getText().toString().trim(), etPassword.getText().toString().trim(), new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError e) {

                    // shake the username and password field, to show the user that theres a mistake
                    Animation shake = AnimationUtils.loadAnimation(context, R.anim.shake);
                    etUsername.startAnimation(shake);
                    etPassword.startAnimation(shake);

                    // enable buttons again
                    loginButton.setEnabled(true);
                    loginButton.setBackgroundColor(0xFF2683FF); // this is the manually copied "colorPrimary" defined in colors.xml
                    newAccountButton.setEnabled(true);
                    newAccountButton.setBackgroundColor(0xFF2683FF); // this is the manually copied "colorPrimary" defined in colors.xml

                    // print stacktrace to log
                    StringWriter error = new StringWriter();
                    e.printStackTrace(new PrintWriter(error));
                    Log.d("Login Error", error.toString());
                }
            });

    }

    @Override
    public void onStart() {
        super.onStart();

        // check whether device has internet access and if not, show an alert.
        DeviceOnlineChecker.alertIfOffline(this.getApplicationContext(), InitializationActivity.this.getFragmentManager());

    }

    public void OnNewAccountButtonClick(View view) {

        Uri storjSignUpUrl = Uri.parse(Config.URL_STORJ_SIGN_UP);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, storjSignUpUrl);
        startActivity(launchBrowser);
    }

    public void loginUser(final String storjUsername, String storjPassword, Response.ErrorListener errorListener) {

        final SharedPreferences sharedPreferencesAuthentication = getApplicationContext().getApplicationContext().getSharedPreferences(Config.SHARED_PREF_AUTHENTICATION, Context.MODE_PRIVATE);
        final StorjRestClient storjRestClient = StorjRestClient.getInstance(Config.URL_BRIDGE_API, this.getApplicationContext(), new FailedSignatureAuthenticationListener(), sharedPreferencesAuthentication);

        // Create KeyPair for future authentication
        final CryptoUtils cryptoUtils = new CryptoUtils(getBaseContext(), sharedPreferencesAuthentication);
        cryptoUtils.initializeSignatureBasedAuthentication();

        storjRestClient.registerPublicKey(storjUsername, storjPassword, cryptoUtils.loadPublicKey(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                // store username in shared preferences
                SharedPreferences.Editor spEditor = sharedPreferencesAuthentication.edit();
                spEditor.putString(Config.SHARED_PREF_VAR_USERNAME, storjUsername);
                spEditor.commit();

                if(!(new SeedUtils(getBaseContext()).seedInformationIsStored())){
                    importSeed();
                }
            }
        }, errorListener);
    }

    @Override
    protected void onStop() {
        super.onStop();

        // todo still not implemented
// Request stoppen // https://developer.android.com/training/volley/simple.html
//        if (mRequestQueue != null) {
//            mRequestQueue.cancelAll(TAG);
//        }

    }

    protected void importSeed(){
        this.setContentView(R.layout.seed_import);

        importSeedButton = (Button) findViewById(R.id.import_seed);
        importSeedButton.setEnabled(false);
        importSeedButton.setBackgroundColor(Color.GRAY);

        seedNotValid = (TextView) findViewById(R.id.labelSeedInvalid);
        seedNotValid.setVisibility(View.INVISIBLE);

        wordTextfields = new ArrayList<EditText>();
        wordTextfields.add((EditText) findViewById(R.id.mnemonic_word1));
        wordTextfields.add((EditText) findViewById(R.id.mnemonic_word2));
        wordTextfields.add((EditText) findViewById(R.id.mnemonic_word3));
        wordTextfields.add((EditText) findViewById(R.id.mnemonic_word4));
        wordTextfields.add((EditText) findViewById(R.id.mnemonic_word5));
        wordTextfields.add((EditText) findViewById(R.id.mnemonic_word6));
        wordTextfields.add((EditText) findViewById(R.id.mnemonic_word7));
        wordTextfields.add((EditText) findViewById(R.id.mnemonic_word8));
        wordTextfields.add((EditText) findViewById(R.id.mnemonic_word9));
        wordTextfields.add((EditText) findViewById(R.id.mnemonic_word10));
        wordTextfields.add((EditText) findViewById(R.id.mnemonic_word11));
        wordTextfields.add((EditText) findViewById(R.id.mnemonic_word12));

        // change color to red if words have been entered wrongly
        for(EditText wordTextfield : wordTextfields){
            wordTextfield.setOnFocusChangeListener(new View.OnFocusChangeListener(){
                @Override
                public void onFocusChange(View v, boolean hasFocus){
                    colorizeMnemonicErrors(v, hasFocus);
                }
            });
            wordTextfield.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    buttonEnabling();
                }
            });
        }

    }

    public void OnImportSeedButtonClick(View view){

        String seedAsString = android.text.TextUtils.join(" ", getSeedWords()); // from list to string

        try {
            new SeedUtils(getBaseContext()).importMnemonic(seedAsString, true);

            //  signature-based authentication is already installed plus encryption seed is installed - start BucketsActivity
            Intent intent = new Intent(this, BucketsActivity.class);
            startActivity(intent);
        } catch (MnemonicException e) {

            // Mnemonic was not valid, so we let the user know ;-)
            // --> makes all words red and disables import button

            seedChecksumJustFailed = true;
            seedNotValid.setVisibility(View.VISIBLE);
            importSeedButton.setEnabled(false);
            importSeedButton.setBackgroundColor(Color.GRAY);

            // set all textfields to red
            for(EditText wordTextfield : wordTextfields){
                wordTextfield.setTextColor(Color.RED);
            }

            // print stacktrace to log
            StringWriter error = new StringWriter();
            e.printStackTrace(new PrintWriter(error));
            Log.d("Mnemonic error", error.toString());
        }
    }

    public void buttonEnabling(){

        boolean allWordsAreCorrctlyPut = true;

        // check for each word if it is valid
        for(String word : getSeedWords()){
            if(word.equals("") || !SeedUtils.isWordValid(word)){
                allWordsAreCorrctlyPut = false;
                break;
            }
        }

        if(allWordsAreCorrctlyPut){
            importSeedButton.setEnabled(true);
            importSeedButton.setBackgroundColor(0xFF2683FF); // this is the manually copied "colorPrimary" defined in colors.xml
        }else{
            importSeedButton.setEnabled(false);
            importSeedButton.setBackgroundColor(Color.GRAY);
        }
    }

    public void colorizeMnemonicErrors(View v, boolean hasFocus){

        // if all words are red because the mnemonic validation failed, make them black again as soon as the user clicks one of the fields
        if(seedChecksumJustFailed){

            seedChecksumJustFailed = false;
            seedNotValid.setVisibility(View.INVISIBLE);

            for(EditText wordTextfield : wordTextfields){
                wordTextfield.setTextColor(Color.BLACK);
            }
        }

        // if a word is entered wrongly, make it red, if a red word is clicked, make it blacek
        if (hasFocus){
            ((EditText) v).setTextColor(Color.BLACK);
        }else{
            EditText et = (EditText) v;
            String word = et.getText().toString().trim().toLowerCase();

            if(!word.equals("")){
                if(!SeedUtils.isWordValid(word)){
                    ((EditText) v).setTextColor(Color.RED);
                }
            }
        }
    }

    /**
     * reads words from textfields, puts them to lowercase and trims them
     * @return List of words [Strings]
     */
    protected ArrayList<String> getSeedWords(){
        ArrayList<String> ar = new ArrayList<String>();
        for(EditText wordTextfield : wordTextfields){
            ar.add(wordTextfield.getText().toString().trim().toLowerCase());
        }
        return ar;
    }
}

