package adapters;

/**
 * Created by Juho on 12.10.2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.storjAndroidClient.restClient.model.BucketEntry;
import com.storjAndroidClient.storjandroid.R;

import java.util.List;

public class EntryArrayAdapter extends ArrayAdapter<BucketEntry> {
    private final Context context;
    private final List<BucketEntry> bucketEntryList;
    final SharedPreferences prefs;
    final SharedPreferences.Editor edit;

    public EntryArrayAdapter(Context context, List<BucketEntry> entryList) {
        super(context, R.layout.entrylist, entryList);
        this.context = context;
        this.bucketEntryList = entryList;
        prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        edit = prefs.edit();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.entrylist, parent, false);

        TextView textView = (TextView) rowView.findViewById(R.id.Entryname);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

        textView.setText(bucketEntryList.get(position).getFilename());

        int size = prefs.getInt("entryTextSize", 100);

        ViewGroup.LayoutParams params = textView.getLayoutParams();
        ViewGroup.LayoutParams params2 = imageView.getLayoutParams();

        params2.height = size;
        params.height = size;

        imageView.setLayoutParams(params2);
        textView.setLayoutParams(params);

        // Change icon based on name
        String s = bucketEntryList.get(position).getFilename();

        String extension = "";
        int i = s.lastIndexOf('.');
        if (i >= 0) {
            extension = s.substring(i+1).toLowerCase();
        }

        //TODO Add more options to switch case
        switch(extension){
            case "aac":
                imageView.setImageResource(R.drawable.ft_aac);
                break;
            case "ai":
                imageView.setImageResource(R.drawable.ft_ai);
                break;
            case "aiff":
                imageView.setImageResource(R.drawable.ft_aiff);
                break;
            case "avi":
                imageView.setImageResource(R.drawable.ft_avi);
                break;
            case "bmp":
                imageView.setImageResource(R.drawable.ft_bmp);
                break;
            case "c":
                imageView.setImageResource(R.drawable.ft_c);
                break;
            case "cpp":
                imageView.setImageResource(R.drawable.ft_cpp);
                break;
            case "css":
                imageView.setImageResource(R.drawable.ft_css);
                break;
            case "dat":
                imageView.setImageResource(R.drawable.ft_dat);
                break;
            case "dmg":
                imageView.setImageResource(R.drawable.ft_dmg);
                break;
            case "doc":
                imageView.setImageResource(R.drawable.ft_doc);
                break;
            case "docx":
                imageView.setImageResource(R.drawable.ft_docx);
                break;
            case "dotx":
                imageView.setImageResource(R.drawable.ft_dotx);
                break;
            case "dwg":
                imageView.setImageResource(R.drawable.ft_dwg);
                break;
            case "dxf":
                imageView.setImageResource(R.drawable.ft_dxf);
                break;
            case "eps":
                imageView.setImageResource(R.drawable.ft_eps);
                break;
            case "exe":
                imageView.setImageResource(R.drawable.ft_exe);
                break;
            case "flv":
                imageView.setImageResource(R.drawable.ft_flv);
                break;
            case "h":
                imageView.setImageResource(R.drawable.ft_h);
                break;
            case "hpp":
                imageView.setImageResource(R.drawable.ft_hpp);
                break;
            case "html":
                imageView.setImageResource(R.drawable.ft_html);
                break;
            case "ics":
                imageView.setImageResource(R.drawable.ft_ics);
                break;
            case "iso":
                imageView.setImageResource(R.drawable.ft_iso);
                break;
            case "jar":
            case "class":
            case "java":
                imageView.setImageResource(R.drawable.ft_java);
                break;
            case "jpg":
            case "jpeg":
                imageView.setImageResource(R.drawable.ft_jpg);
                break;
            case "js":
                imageView.setImageResource(R.drawable.ft_js);
                break;
            case "key":
                imageView.setImageResource(R.drawable.ft_key);
                break;
            case "less":
                imageView.setImageResource(R.drawable.ft_less);
                break;
            case "mid":
                imageView.setImageResource(R.drawable.ft_mid);
                break;
            case "mov":
                imageView.setImageResource(R.drawable.ft_mov);
                break;
            case "mp3":
                imageView.setImageResource(R.drawable.ft_mp3);
                break;
            case "mp4":
                imageView.setImageResource(R.drawable.ft_mp4);
                break;
            case "mpeg":
            case "mpg":
                imageView.setImageResource(R.drawable.ft_mpg);
                break;
            case "odf":
                imageView.setImageResource(R.drawable.ft_odf);
                break;
            case "ods":
                imageView.setImageResource(R.drawable.ft_ods);
                break;
            case "odt":
                imageView.setImageResource(R.drawable.ft_odt);
                break;
            case "otp":
                imageView.setImageResource(R.drawable.ft_otp);
                break;
            case "ots":
                imageView.setImageResource(R.drawable.ft_ots);
                break;
            case "ott":
                imageView.setImageResource(R.drawable.ft_ott);
                break;
            case "page":
                imageView.setImageResource(R.drawable.ft_page);
                break;
            case "pdf":
                imageView.setImageResource(R.drawable.ft_pdf);
                break;
            case "php":
                imageView.setImageResource(R.drawable.ft_php);
                break;
            case "png":
                imageView.setImageResource(R.drawable.ft_png);
                break;
            case "ppt":
                imageView.setImageResource(R.drawable.ft_ppt);
                break;
            case "pptx":
                imageView.setImageResource(R.drawable.ft_pptx);
                break;
            case "psd":
                imageView.setImageResource(R.drawable.ft_psd);
                break;
            case "py":
                imageView.setImageResource(R.drawable.ft_py);
                break;
            case "qt":
                imageView.setImageResource(R.drawable.ft_qt);
                break;
            case "rar":
                imageView.setImageResource(R.drawable.ft_rar);
                break;
            case "rb":
                imageView.setImageResource(R.drawable.ft_rb);
                break;
            case "rm":
                imageView.setImageResource(R.drawable.ft_rm);
                break;
            case "rtf":
                imageView.setImageResource(R.drawable.ft_rtf);
                break;
            case "sass":
                imageView.setImageResource(R.drawable.ft_sass);
                break;
            case "scss":
                imageView.setImageResource(R.drawable.ft_scss);
                break;
            case "sql":
                imageView.setImageResource(R.drawable.ft_sql);
                break;
            case "swf":
                imageView.setImageResource(R.drawable.ft_swf);
                break;
            case "tga":
                imageView.setImageResource(R.drawable.ft_tga);
                break;
            case "tgz":
                imageView.setImageResource(R.drawable.ft_tgz);
                break;
            case "tiff":
                imageView.setImageResource(R.drawable.ft_tiff);
                break;
            case "text":
            case "txt":
                imageView.setImageResource(R.drawable.ft_txt);
                break;
            case "wav":
                imageView.setImageResource(R.drawable.ft_wav);
                break;
            case "wmv":
                imageView.setImageResource(R.drawable.ft_wmv);
                break;
            case "xls":
                imageView.setImageResource(R.drawable.ft_xls);
                break;
            case "xlsx":
                imageView.setImageResource(R.drawable.ft_xlsx);
                break;
            case "xml":
                imageView.setImageResource(R.drawable.ft_xml);
                break;
            case "yml":
                imageView.setImageResource(R.drawable.ft_yml);
                break;
            case "zip":
                imageView.setImageResource(R.drawable.ft_zip);
                break;
            default:
                imageView.setImageResource(R.drawable.ft_blank);
                break;
        }

        return rowView;
    }
}