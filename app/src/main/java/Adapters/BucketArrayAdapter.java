package adapters;

/**
 * Created by Juho on 12.10.2016.
 */
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.storjAndroidClient.restClient.model.Bucket;
import com.storjAndroidClient.storjandroid.R;

import java.util.List;

public class BucketArrayAdapter extends ArrayAdapter<Bucket> {
    private final Context context;
    private final List<Bucket> bucketsList;
    final SharedPreferences prefs;
    final SharedPreferences.Editor edit;

    public BucketArrayAdapter(Context context, List<Bucket> buckets) {
        super(context, R.layout.bucketlist, buckets);
        this.context = context;
        this.bucketsList = buckets;
        prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        edit = prefs.edit();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.bucketlist, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.Bucketname);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        ImageView globeView = (ImageView) rowView.findViewById(R.id.globe_icon);
        textView.setText(bucketsList.get(position).getName());
        int size = prefs.getInt("bucketTextSize", 110);
        ViewGroup.LayoutParams params = textView.getLayoutParams();
        ViewGroup.LayoutParams params2 = imageView.getLayoutParams();
        params2.height = size;
        params.height = size;
        imageView.setLayoutParams(params2);
        textView.setLayoutParams(params);

        // hide globe icon from buckets that are NOT public buckets
        if(bucketsList.get(position).getEncryptionKey() == null || bucketsList.get(position).getEncryptionKey().equals("")){
            globeView.setVisibility(View.INVISIBLE);
        }

        return rowView;
    }
}