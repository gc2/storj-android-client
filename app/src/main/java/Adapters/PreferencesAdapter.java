package adapters;

/**
 * Created by Juho on 18.10.2016.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.storjAndroidClient.storjandroid.R;

public class PreferencesAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] items;

    public PreferencesAdapter(Context context, String[] items) {
        super(context, R.layout.preferenceslist, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.preferenceslist, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.ItemName);
        textView.setText(items[position]);

        return rowView;
    }
}