package com.storjAndroidClient.storjandroid;

/**
 * Created by Gabriel Comte on 28.2.2017.
 */

public class Config {

    // weblinks
    public static final String URL_BRIDGE_API = "https://api.storj.io";
    public static final String URL_STORJ_SIGN_UP = "https://app.storj.io/#/signup";

    // shared preferences files
    public static final String SHARED_PREF_AUTHENTICATION = "storjAuthentication";

    // shared preferences variables
    public static final String SHARED_PREF_VAR_USERNAME = "username";
}
