package environmentCheck;

import android.app.FragmentManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import dialogs.DeviceOfflineDialogFragment;

/**
 * Created by Gabriel Comte on 02.12.2016.
 */

public class DeviceOnlineChecker {

    /**
     * @param context
     * @return boolean true if device is connected to the internet | false if device is not connected to the internet
     */
    public static boolean deviceOnline(Context context){

        // check internet connection
        // uses permission android.permission.ACCESS_NETWORK_STATE
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    /**
     * @param context
     * @return boolean true if device is connected to the internet | false if device is not connected to the internet
     */
    public static boolean alertIfOffline(Context context, FragmentManager fm){
        if(!DeviceOnlineChecker.deviceOnline(context)){
            DeviceOfflineDialogFragment deviceOfflineDialog = new DeviceOfflineDialogFragment();
            deviceOfflineDialog.show(fm, "deviceOfflineDialog");
            return false;
        }
        return true;
    }


}
