package implementedListeners;

import com.storjAndroidClient.restClient.backgroundServices.templateClasses.AllFilesTransferredListener;

/**
 * Created by Gabriel Comte on 15.11.2016.
 */

public class AllFilesDownloadedListenerImplementation implements AllFilesTransferredListener {
    @Override
    public void onFileTransferOver() {
        System.out.println("All files downloaded! [message from InspectBucketActivity class / AllFilesDownloadedListenerImplementation]");
    }
}
