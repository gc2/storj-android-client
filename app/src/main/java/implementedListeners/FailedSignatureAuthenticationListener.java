package implementedListeners;

import android.content.Context;
import android.content.Intent;

import com.storjAndroidClient.restClient.auth.AuthenticationFailedListener;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;
import com.storjAndroidClient.storjandroid.Config;

import activities.InitializationActivity;

/**
 * Created by Gabriel Comte on 18.1.2017.
 */

public class FailedSignatureAuthenticationListener implements AuthenticationFailedListener {

    @Override
    public void onFailedAuthentication(Context context) {
        logout(context);
    }

    public void logout(Context context){

        // remove the keys from shared preferences and redirect to InitializationActivity.
        // this will lead to the situation, that the user has to login, as if he started the application for the first time

        CryptoUtils cryptoUtils = new CryptoUtils(context, context.getSharedPreferences(Config.SHARED_PREF_AUTHENTICATION, Context.MODE_PRIVATE));
        cryptoUtils.resetAuthenticationKeys();

        //  signature-based authentication is already installed - start BucketsActivity
        Intent intent = new Intent(context, InitializationActivity.class);
        context.startActivity(intent);
    }
}
