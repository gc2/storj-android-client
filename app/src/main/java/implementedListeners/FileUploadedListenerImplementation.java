package implementedListeners;

import android.support.v7.app.AppCompatActivity;

import com.storjAndroidClient.restClient.backgroundServices.templateClasses.FileTransferListener;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.QueuedTransfer;
import com.storjAndroidClient.restClient.model.BucketEntry;

import activities.InspectBucketActivity;

/**
 * Created by Gabriel Comte on 07.11.2016.
 */

public class FileUploadedListenerImplementation implements FileTransferListener {

    AppCompatActivity activity;

    public FileUploadedListenerImplementation(AppCompatActivity activity){
        this.activity = activity;
    }

    @Override
    public void onFileTransferred(QueuedTransfer queuedTransfer) {

        ((InspectBucketActivity) activity).refreshBucketList();

        System.out.println("File uploaded! [message from InspectBucketActivity class / FileUploadedListenerImplementation]");
    }

    @Override
    public void onTransferFailed(QueuedTransfer queuedTransfer) {
        // todo!!!

        System.out.println("File upload ERROR!!! [message from InspectBucketActivity class / FileuploadedListenerImplementation]");
    }
}
