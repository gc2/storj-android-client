package implementedListeners;

import android.app.DownloadManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.storjAndroidClient.restClient.backgroundServices.download.QueuedDownload;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.FileTransferListener;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.QueuedTransfer;

/**
 * Created by Gabriel Comte on 15.11.2016.
 */

public class FileDownloadedListenerImplementation implements FileTransferListener {

    AppCompatActivity activity;

    public FileDownloadedListenerImplementation(AppCompatActivity activity){
        this.activity = activity;
    }

    @Override
    public void onFileTransferred(QueuedTransfer queuedTransfer) {

        QueuedDownload qd = (QueuedDownload) queuedTransfer;

        System.out.println("File downloaded! [message from InspectBucketActivity class / FileDownloadedListenerImplementation]");

        // Add Download to the Android Download Manager
        DownloadManager androidDownloadManager = (DownloadManager) this.activity.getBaseContext().getSystemService(Context.DOWNLOAD_SERVICE);

        androidDownloadManager.addCompletedDownload(qd.getUnencryptedFile().getName(), "from Storj", true, qd.getMimeType(), qd.getUnencryptedFile().getPath(), qd.getUnencryptedFile().length(), true);
    }

    @Override
    public void onTransferFailed(QueuedTransfer queuedTransfer) {
        // todo!!!

        System.out.println("File download ERROR!!! [message from InspectBucketActivity class / FileDownloadedListenerImplementation]");
    }
}
