package com.storjAndroidClient.restClient;

/**
 * Created by Gabriel Comte on 03.10.2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.util.ArrayMap;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.storjAndroidClient.restClient.auth.AuthenticationFailedListener;
import com.storjAndroidClient.restClient.auth.BasicAuthTypeJsonObjectRequest;
import com.storjAndroidClient.restClient.auth.SignatureAuthTypeJsonObjectRequest;
import com.storjAndroidClient.restClient.auth.SignatureAuthTypeStringRequest;
import com.storjAndroidClient.restClient.backgroundServices.download.FileMuxerRunnable;
import com.storjAndroidClient.restClient.backgroundServices.download.FileMuxingManager;
import com.storjAndroidClient.restClient.backgroundServices.download.QueuedDownload;
import com.storjAndroidClient.restClient.backgroundServices.download.ShardDownloadingManager;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.AllFilesTransferredListener;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.FileTransferListener;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.QueuedTransfer;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.Shard;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.ShardStatus;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.TransferStatus;
import com.storjAndroidClient.restClient.backgroundServices.upload.FileDemuxerRunnable;
import com.storjAndroidClient.restClient.backgroundServices.upload.FileDemuxingManager;
import com.storjAndroidClient.restClient.backgroundServices.upload.QueuedUpload;
import com.storjAndroidClient.restClient.backgroundServices.upload.ShardUploadingManager;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;
import com.storjAndroidClient.restClient.fileHandling.encryption.SeedUtils;
import com.storjAndroidClient.restClient.helperClasses.MimeTypeGetter;
import com.storjAndroidClient.restClient.model.Bucket;
import com.storjAndroidClient.restClient.model.BucketEntry;
import com.storjAndroidClient.restClient.model.Frame;
import com.storjAndroidClient.restClient.model.Operation;
import com.storjAndroidClient.restClient.model.PublicBucketPermissions;
import com.storjAndroidClient.restClient.model.ShardPointer;
import com.storjAndroidClient.restClient.model.Token;
import com.storjAndroidClient.restClient.model.UploadableShard;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

public class StorjRestClient {

    private static StorjRestClient myInstance;

    protected Context context;
    protected AuthenticationFailedListener authenticationFailedListener;
    protected CryptoUtils cryptoUtils;

    protected Gson gson;

    // Storj API root.
    private String apiRoot;

    // Contact end points
    private String storjApiContacts;

    // User end points.
    private String storjApiUsers;
    private String storjApiUsersPassReset;
    private String storjApiUsersActivate;
    private String storjApiUsersDeactivate;

    // Keys
    private String storjApiKeys;

    // Frames
    private String storjApiFrames;

    // Buckets
    private String storjApiBuckets;

    /// Exchange Reports
    private static String storjApiExchangeReports;

    protected String jsonBuckets;
    protected String currentFileList;

    private List<QueuedTransfer> transferList;

    private StorjRestClient(String apiUrl, Context context, AuthenticationFailedListener authenticationFailedListener, SharedPreferences sharedPreferencesAuth){

        this.transferList = new ArrayList<QueuedTransfer>();

        this.apiRoot = apiUrl;
        this.storjApiKeys = apiRoot + "/keys";
        this.storjApiFrames = apiRoot + "/frames";
        this.storjApiBuckets = apiRoot + "/buckets";
        this.storjApiExchangeReports = apiRoot + "/reports/exchanges";

        gson = new GsonBuilder().create();
        this.context = context;
        this.authenticationFailedListener = authenticationFailedListener;
        this.cryptoUtils = new CryptoUtils(context, sharedPreferencesAuth);
    }

    public static synchronized StorjRestClient getInstance(String apiUrl, Context context, AuthenticationFailedListener authenticationFailedListener, SharedPreferences sharedPreferencesAuth) {
        if (myInstance == null) {
            myInstance = new StorjRestClient(apiUrl, context, authenticationFailedListener, sharedPreferencesAuth);
        }

        return myInstance;
    }

    public static synchronized StorjRestClient getInstance() throws Exception {
        if (myInstance == null) {
            throw new Exception("StorjRestClient must first be initialized before the instance may be retrieved without delivering parameters");
        }

        return myInstance;
    }


    /**
     * @param url
     * @param responseListener
     */

    public void sendGetRequest(String url, Response.Listener<String> responseListener){
        sendGetRequest(url, responseListener, null);

        String nonce = UUID.randomUUID().toString();

        // Request a string response from the provided URL.
        SignatureAuthTypeStringRequest stringRequest = new SignatureAuthTypeStringRequest(Request.Method.GET, apiRoot, url + "?__nonce=" + nonce, nonce, cryptoUtils, responseListener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                // if authentication failed, let user reauthenticate
                if(error instanceof com.android.volley.AuthFailureError) {
                    authenticationFailedListener.onFailedAuthentication(context);
                }else {
                    error.printStackTrace();
                }
            }

        });

        // Add the request to the RequestQueue.
        StorjRequestSingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


    /**
     *
     * @param url
     * @param mHeaders
     * @param responseListener
     */
    public void sendGetRequest(String url, Response.Listener<String> responseListener, Map<String, String> mHeaders){

        String nonce = UUID.randomUUID().toString();

        // Request a string response from the provided URL.
        SignatureAuthTypeStringRequest stringRequest = new SignatureAuthTypeStringRequest(Request.Method.GET, apiRoot, url + "?__nonce=" + nonce, nonce, cryptoUtils, responseListener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                // if authentication failed, let user reauthenticate
                if(error instanceof com.android.volley.AuthFailureError) {
                    authenticationFailedListener.onFailedAuthentication(context);
                }else {
                    error.printStackTrace();
                }
            }

        });

        if(mHeaders != null) {

            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            System.out.println(url);
            System.out.println(mHeaders);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");


            stringRequest.addToHeader(mHeaders);
        }

        // Add the request to the RequestQueue.
        StorjRequestSingleton.getInstance(context).addToRequestQueue(stringRequest);
    }



    /**
     * @param url
     * @param parameterData
     * @param responseListener
     * @throws JSONException
     */
    public void sendPostRequest(String url, JSONObject parameterData, Response.Listener<JSONObject> responseListener) throws JSONException {

        parameterData.put("__nonce", UUID.randomUUID().toString());

        // Request a string response from the provided URL.
        SignatureAuthTypeJsonObjectRequest jsonObjectRequest = new SignatureAuthTypeJsonObjectRequest(Request.Method.POST, apiRoot, url, cryptoUtils, parameterData, responseListener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // if authentication failed, let user reauthenticate
                if(error instanceof com.android.volley.AuthFailureError) {
                    authenticationFailedListener.onFailedAuthentication(context);
                }else {
                    error.printStackTrace();
                }
            }
        });

        // Add the request to the RequestQueue.
        StorjRequestSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    /**
     * @param url
     * @param parameterData
     * @param responseListener
     * @throws JSONException
     */
    public void sendPostRequest(String url, String parameterData, Response.Listener<JSONObject> responseListener) throws JSONException {
        sendPostRequest(url, new JSONObject(parameterData), responseListener);
    }

    /**
     * @param storjUsername
     * @param storjPassword
     * @param url
     * @param parameterData
     * @param context
     * @param responseListener
     * @param errorListener
     * @throws JSONException
     */
    public static void sendBasicAuthPostRequest(String storjUsername, String storjPassword, String url, final JSONObject parameterData, Context context, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) throws JSONException {

        // Request a string response from the provided URL.
        BasicAuthTypeJsonObjectRequest jsonObjectRequest = new BasicAuthTypeJsonObjectRequest(Request.Method.POST, url, parameterData, responseListener, errorListener, storjUsername, storjPassword);

        // Add the request to the RequestQueue.
        StorjRequestSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    /**
     * @param url
     * @param parameterData
     * @param responseListener
     * @param authenticationFailedListener
     * @throws JSONException
     */
    public void sendPatchRequest(String url, String parameterData, final Response.Listener<JSONObject> responseListener, final AuthenticationFailedListener authenticationFailedListener) throws JSONException {

        JSONObject jsonBody = new JSONObject(parameterData);
        jsonBody.put("__nonce", UUID.randomUUID().toString());

        // Request a string response from the provided URL.
        SignatureAuthTypeJsonObjectRequest jsonObjectRequest = new SignatureAuthTypeJsonObjectRequest(Request.Method.PATCH, apiRoot, url, cryptoUtils, jsonBody, responseListener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // if authentication failed, let user reauthenticate
                if(error instanceof com.android.volley.AuthFailureError) {
                    authenticationFailedListener.onFailedAuthentication(context);
                }else {
                    error.printStackTrace();
                }
            }
        });

        // Add the request to the RequestQueue.
        StorjRequestSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    /**
     * @param url
     * @param parameterData
     * @param responseListener
     * @throws JSONException
     */
    public void sendPutRequest(String url, String parameterData, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) throws JSONException {
        sendPutRequest(url, new JSONObject(parameterData), responseListener, errorListener);
    }

    /**
     * @param url
     * @param parameterData
     * @param responseListener
     * @throws JSONException
     */
    public void sendPutRequest(String url, JSONObject parameterData, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) throws JSONException {

        parameterData.put("__nonce", UUID.randomUUID().toString());

        // Request a string response from the provided URL.
        SignatureAuthTypeJsonObjectRequest jsonObjectRequest = new SignatureAuthTypeJsonObjectRequest(Request.Method.PUT, apiRoot, url, cryptoUtils, parameterData, responseListener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // if authentication failed, let user reauthenticate
                if (error instanceof com.android.volley.AuthFailureError) {
                    authenticationFailedListener.onFailedAuthentication(context);
                } else {
                    errorListener.onErrorResponse(error);
                }
            }
        });

        // Add the request to the RequestQueue.
        StorjRequestSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    /**
     *
     * @param url of the api which the object that should be deleted belongs to
     * @param objectId Id of the object that should be deleted
     * @param responseListener
     */
    public void sendDeleteRequest(String url, String objectId, final Response.Listener<String> responseListener, final AuthenticationFailedListener authenticationFailedListener){

        String nonce = UUID.randomUUID().toString();
        String requestUrl = url + "/" + objectId + "?__nonce=" + nonce;

        // Request a string response from the provided URL.
        SignatureAuthTypeStringRequest stringRequest = new SignatureAuthTypeStringRequest(Request.Method.DELETE, apiRoot, requestUrl, nonce, cryptoUtils, responseListener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // if authentication failed, let user reauthenticate
                if (error instanceof com.android.volley.AuthFailureError) {
                    authenticationFailedListener.onFailedAuthentication(context);
                } else {
                    error.printStackTrace();
                }
            }
        });

        // Add the request to the RequestQueue.
        StorjRequestSingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


    /***************************************************************************************/
    /***********************          AUTHENTICATION KEYS            ***********************/
    /***************************************************************************************/

    // registers the public key to the user on the bridge
    public void registerPublicKey(String storjUsername, String storjPassword, String publicKeyForBridge, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {

        JSONObject publicKeyJson = new JSONObject();
        try {
            publicKeyJson.put("key",publicKeyForBridge);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            sendBasicAuthPostRequest(storjUsername, storjPassword, storjApiKeys, publicKeyJson, this.context, responseListener, errorListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    /***************************************************************************************/
    /***********************                 BUCKETS                 ***********************/
    /***************************************************************************************/

    /**
     * Get a list of all the buckets the user has access to.
     *
     * @return the list of buckets.
     */
    public void loadBucketList(final Response.Listener<String> responseListener) {

        Response.Listener<String> newResponseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                jsonBuckets = response;
                responseListener.onResponse(response);
            }
        };
        try {
            sendGetRequest(storjApiBuckets, newResponseListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Get a list of all the buckets the user has access to.
     *
     * @return the list of buckets.
     */
    public List<Bucket> getAllBuckets() throws Exception {

        if(this.jsonBuckets != null && !this.jsonBuckets.equals("")){
            // request response has been received already
            return gson.fromJson(this.jsonBuckets, new TypeToken<List<Bucket>>(){}.getType());
        }else{
            throw new Exception("loadBucketList() must have been called and terminated at least once before calling getAllBuckets()");
        }
    }

    /**
     * Retrieves a bucket by its ID.
     *
     * @param bucketId String the ID of the bucket.
     * @return Bucket The bucket.
     */
    public Bucket getBucketById(String bucketId) throws Exception {
        for(Bucket bucket : this.getAllBuckets()){
            if(bucket.getId().equals(bucketId)){
                return bucket;
            }
        }
        return null;
    }

    /**
     * Creates a bucket on the network
     *
     * @param bucket The bucket to create.
     */
    public void createBucket(Bucket bucket, Response.Listener<JSONObject> responseListener) {
        String bucketJson = gson.toJson(bucket);

        try {
            sendPostRequest(storjApiBuckets, bucketJson, responseListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * Updates a bucket on the network
     *
     * @param bucket The bucket to be updated.
     */
    public void updateBucket(Bucket bucket, SharedPreferences sharedPreferences, Response.Listener<JSONObject> responseListener, AuthenticationFailedListener authenticationFailedListener) {
        String bucketUrl = storjApiBuckets + "/" + bucket.getId();
        String bucketJson = gson.toJson(bucket);
        try {
            this.sendPatchRequest(bucketUrl, bucketJson, responseListener, authenticationFailedListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes a bucket on the network
     *
     * @param bucketId String The bucket id of the bucket to be deleted.
     * @param responseListener What to do when action has been finished.
     */
    public void deleteBucket(String bucketId, SharedPreferences sharedPreferences, Response.Listener<String> responseListener, AuthenticationFailedListener authenticationFailedListener) {
        sendDeleteRequest(storjApiBuckets, bucketId, responseListener, authenticationFailedListener);
    }

    /**
     * Deletes a bucket on the network
     *
     * @param bucket Bucket The bucket to be deleted.
     * @param responseListener What to do when action has been finished.
     */
    public void deleteBucket(Bucket bucket, SharedPreferences sharedPreferences, Response.Listener<String> responseListener, AuthenticationFailedListener authenticationFailedListener) {
        if(bucket.getId() != null && !bucket.getId().isEmpty()) {
            this.deleteBucket(bucket.getId(), sharedPreferences, responseListener, authenticationFailedListener);
        }else{
            try {
                throw new Exception("Bucket does not contain any id!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void makeBucketPublic(Bucket bucket, List<PublicBucketPermissions> permissions, Response.Listener<JSONObject> responseListener, AuthenticationFailedListener authenticationFailedListener){

        String bucketKey = CryptoUtils.bytesToHex(CryptoUtils.generateBucketKey(new SeedUtils(context).getSeed(), bucket.getId()));

        bucket.setEncryptionKey(bucketKey);
        bucket.setPublicPermissionsAsEnums(permissions);

        String bucketUrl = storjApiBuckets + "/" + bucket.getId();

        try {
            this.sendPatchRequest(bucketUrl, gson.toJson(bucket), responseListener, authenticationFailedListener);
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void makeBucketPublic(String bucketId, List<PublicBucketPermissions> permissions, Response.Listener<JSONObject> responseListener, AuthenticationFailedListener authenticationFailedListener) throws Exception {
        this.makeBucketPublic(this.getBucketById(bucketId), permissions, responseListener, authenticationFailedListener);
    }

    public void changePublicBucketPermissions(Bucket bucket, List<PublicBucketPermissions> newPermissions, Response.Listener<JSONObject> responseListener, AuthenticationFailedListener authenticationFailedListener){

        bucket.setPublicPermissionsAsEnums(newPermissions);

        String bucketUrl = storjApiBuckets + "/" + bucket.getId();

        try {
            this.sendPatchRequest(bucketUrl, gson.toJson(bucket), responseListener, authenticationFailedListener);
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void changePublicBucketPermissions(String bucketId, List<PublicBucketPermissions> newPermissions, Response.Listener<JSONObject> responseListener, AuthenticationFailedListener authenticationFailedListener) throws Exception {
        this.changePublicBucketPermissions(this.getBucketById(bucketId), newPermissions, responseListener, authenticationFailedListener);
    }

    /***************************************************************************************/
    /***********************                 FILES                 ***********************/
    /***************************************************************************************/

    /**
     * Get a list of all the buckets the user has access to.
     *
     * @return the list of buckets.
     */
    public void loadFileListByBucketId(String bucketId, final Response.Listener<String> responseListener) {
        String requestUrl = storjApiBuckets + "/" + bucketId + "/" + "files";
        Response.Listener<String> newResponseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                currentFileList = response;
                System.out.println(response);
                responseListener.onResponse(response);
            }
        };
        try {
            StorjRestClient.getInstance().sendGetRequest(requestUrl, newResponseListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get a list of the metadata of all the files which have been loaded by using getFilesOfCurrentFileList()
     *
     * @return the list of files (FileEntries).
     */
    public List<BucketEntry> getFilesOfCurrentFileList() throws Exception {
        if(this.currentFileList != null){
            // request response has been received already
            return gson.fromJson(this.currentFileList, new TypeToken<List<BucketEntry>>(){}.getType());
        }else{
            throw new Exception("loadFileListByBucketId() must have been called and terminated at least once before calling getAllBuckets()");
        }
    }

    /**
     * Retrieves a file (BucketEntry) by its ID.
     *
     * @param bucketEntryId String the ID of the bucket.
     * @return BucketEntry The file with the given ID.
     */
    public BucketEntry getBucketEntryById(String bucketEntryId) throws Exception {
        for(BucketEntry bucketEntry : this.getFilesOfCurrentFileList()){
            if(bucketEntry.getId().equals(bucketEntryId)){
                return bucketEntry;
            }
        }
        return null;
    }

    public void uploadFile(File file, Bucket bucket, FileTransferListener fileTransferListener, AllFilesTransferredListener allFilesTransferredListener) throws Exception {
        uploadFile(file, bucket.getId(), fileTransferListener, allFilesTransferredListener);
    }

    public void uploadFile(final File inputFile, final String bucketId, final FileTransferListener fileTransferListener, final AllFilesTransferredListener allFilesTransferredListener) {

        new Thread(){

            @Override
            public void run() {

                Log.d("File Upload", "starting file upload for file '" + inputFile.getName() + "' [Thread: " + Thread.currentThread().getName() + ", Thread id: " + Thread.currentThread().getId() + "]");

                // todo this should be part of the app, not of the library [all of context should be app related]
                File externalCacheDir = context.getExternalCacheDir();
                SharedPreferences sharedPreferencesAuthentication = context.getApplicationContext().getSharedPreferences("storjAuthentication", Context.MODE_PRIVATE);

                File encryptedFile = new File(externalCacheDir.getPath() + File.separator + inputFile.getName() + ".encrypted"); // split filename from path http://stackoverflow.com/questions/13715944/split-path-name-in-android
                final QueuedUpload queuedUpload = new QueuedUpload(inputFile, encryptedFile, bucketId, null, null);  //file id and failedTransferListener are set later

                transferList.add(queuedUpload);


                queuedUpload.setFileTransferListener(new FileTransferListener() {
                    @Override
                    public void onFileTransferred(QueuedTransfer queuedTransfer) {
                        fileTransferListener.onFileTransferred(queuedTransfer);
                    }

                    @Override
                    public void onTransferFailed(QueuedTransfer queuedTransfer) {
                        cleanupOnUploadFailed(queuedTransfer);
                        fileTransferListener.onTransferFailed(queuedTransfer);
                    }
                });

                // Demux the file [encrypt and shard it]
                FileDemuxerRunnable demuxRunnable = new FileDemuxerRunnable(queuedUpload, cryptoUtils, externalCacheDir);
                FileDemuxingManager.getInstance().addToDemuxQueue(demuxRunnable);

                // create Frame
                try {
                    createFrame(sharedPreferencesAuthentication, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            queuedUpload.setFrame(gson.fromJson(response.toString(), Frame.class));
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // add upload to queue
                ShardUploadingManager.getInstance(cryptoUtils).addFileToQueue(queuedUpload, allFilesTransferredListener);

                // wait for all shards to be uploaded and then create a bucket entry
                queuedUpload.waitForAllShardsToBeTransferred();
                if(queuedUpload.getStatus() == TransferStatus.CREATING_BUCKET_ENTRY) {
                    createBucketEntry(queuedUpload);
                }

                try {
                    queuedUpload.getBucketEntryCreatedLatch().await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                queuedUpload.setStatus(TransferStatus.FINISHED);
                queuedUpload.getFileTransferListener().onFileTransferred(queuedUpload);
            }
        }.start();
    }

    private void cleanupOnUploadFailed(QueuedTransfer queuedTransfer){

        Log.d("File upload", "Uploading file '" + queuedTransfer.getUnencryptedFile().getName() + "' failed. Cleaning up. [Thread: " + Thread.currentThread().getName() + ", Thread id: " + Thread.currentThread().getId() + "]");

        // cancel sharding and uploading
        ShardUploadingManager.getInstance(cryptoUtils).cancelAll(queuedTransfer.getTransferId());
        queuedTransfer.setTransferFailed();
        FileDemuxingManager.getInstance().cancelAll(queuedTransfer.getTransferId());

        deleteFilesOnTransferFailed(queuedTransfer);

    }

    private void cleanupOnDownloadFailed(QueuedTransfer queuedTransfer){

        Log.d("File upload", "Uploading file '" + queuedTransfer.getUnencryptedFile().getName() + "' failed. Cleaning up. [Thread: " + Thread.currentThread().getName() + ", Thread id: " + Thread.currentThread().getId() + "]");

        // cancel piecing file together and downloading
        ShardDownloadingManager.getInstance(cryptoUtils).cancelAll(queuedTransfer.getTransferId());
        queuedTransfer.setTransferFailed();
        FileMuxingManager.getInstance().cancelAll(queuedTransfer.getTransferId());

        deleteFilesOnTransferFailed(queuedTransfer);
    }

    private void deleteFilesOnTransferFailed(QueuedTransfer queuedTransfer){
        // delete all shard files
        for(Shard shard : queuedTransfer.getShards()){
            File shardFile = new File(shard.getPath());
            if(shardFile.exists()) {
                shardFile.delete();
            }
        }

        // delete encrypted file
        if(queuedTransfer.getEncryptedFile().exists()){
            queuedTransfer.getEncryptedFile().delete();
        }
    }

    /**
     * Add a uploadableShard to a frame.
     *
     * @param uploadableShard the uploadableShard to add.
     * @param retries the number of times to retry.
     * @return the Frame with the uploadableShard added.
     */
    public void addShardToFrame(final QueuedUpload currentUpload, final UploadableShard uploadableShard, final int retries, final Response.Listener<JSONObject> responseListener, final FileTransferListener fileTransferListener) {

        if(retries > 0) {
            Log.d("Debugging [" + this.getClass().getSimpleName() + ".addShardToFrame()]", "Attempting to negotiate shard contract, attempts left: " + retries);

            try {
                sendPutRequest(storjApiFrames + "/" + currentUpload.getFrame().getId(), gson.toJson(uploadableShard, UploadableShard.class), responseListener, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        try {
                            addShardToFrame(currentUpload, uploadableShard, retries - 1, responseListener, fileTransferListener);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Exception e) {
                Log.d("Debugging [" + this.getClass().getSimpleName() + ".addShardToFrame()]", "Failed with reason: " + e.getMessage());
            }
        }else{
            // Failed to upload UploadableShard. Restarting the whole upload
            Log.d("Debugging [" + this.getClass().getSimpleName() + ".addShardToFrame()]", "Failed to upload " + uploadableShard.getPath());
            currentUpload.setTransferFailed();
            fileTransferListener.onTransferFailed(currentUpload);

            // this latch needs to be released, if not the algorithm is stuck ==> however, set the TransferStatus to FAILED first
            uploadableShard.setTransferStatus(ShardStatus.FAILED);
            uploadableShard.getAddedToFrameLatch().countDown();
        }
    }




    public void downloadFile(BucketEntry bucketEntry, File decryptedOutputFile, FileTransferListener fileTransferListener, AllFilesTransferredListener allFilesTransferredListener) throws Exception {
        this.downloadFile(bucketEntry.getBucket(), bucketEntry.getId(), bucketEntry.getMimetype(), decryptedOutputFile, fileTransferListener, allFilesTransferredListener);
    }

    public void downloadFile(final String bucketId, final String bucketEntryId, final String fileMimeType, final File decryptedOutputFile, final FileTransferListener fileTransferListener, final AllFilesTransferredListener allFilesTransferredListener) throws Exception {


        // Check given parameters
        boolean bucketIdEmpty = bucketId == null || bucketId.equals("");
        boolean bucketEntryIdEmpty = bucketEntryId == null || bucketEntryId.equals("");
        boolean fileMimeTypeEmpty = fileMimeType == null || fileMimeType.equals("");

        if(bucketIdEmpty || bucketEntryIdEmpty || fileMimeTypeEmpty){
            throw new Exception("Download File: Bucket ID, Bucket Entry ID and file MimeType must not be null!");
        }



        new Thread(){

            @Override
            public void run() {

                File encryptedOutputFile = null;
                try {
                    encryptedOutputFile = File.createTempFile(bucketEntryId + "_", "_encrypted", context.getExternalCacheDir());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                final QueuedDownload queuedDownload = new QueuedDownload(decryptedOutputFile, encryptedOutputFile, bucketId, bucketEntryId, fileTransferListener);
                queuedDownload.setMimeType(fileMimeType);
                queuedDownload.setFileTransferListener(new FileTransferListener() {
                    @Override
                    public void onFileTransferred(QueuedTransfer queuedTransfer) {
                        fileTransferListener.onFileTransferred(queuedTransfer);
                    }

                    @Override
                    public void onTransferFailed(QueuedTransfer queuedTransfer) {
                        cleanupOnDownloadFailed(queuedTransfer);
                        fileTransferListener.onTransferFailed(queuedTransfer);
                    }
                });

                transferList.add(queuedDownload);


                final List<ShardPointer>[] shardPointers = new List[1];
                final CountDownLatch shardPointersLoadedLatch = new CountDownLatch(1);


                Log.d("File Download", "starting file download for file '" + decryptedOutputFile.getName() + "' [Thread: " + Thread.currentThread().getName() + ", Thread id: " + Thread.currentThread().getId() + "]");

                queuedDownload.setStatus(TransferStatus.LOADING_TOKENS);
                loadTokenForBucket(bucketId, Operation.PULL, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject responseObject) {

                        Thread loadShardPointersThread = new Thread() {
                            @Override
                            public void run() {
                                Thread currentThread = Thread.currentThread();
                                Log.d("Executing Thread", currentThread.getName() + ", id = " + currentThread.getId());

                                Token token = gson.fromJson(responseObject.toString(), Token.class);

                                System.out.println(responseObject.toString());
                                System.out.println(token);
                                System.out.println(token.getExpiresAsDateTime());
                                System.out.println("getToken(): " + token.getToken());

                                loadShardPointers(bucketId, bucketEntryId, token.getToken(), new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(final String response) {
                                        shardPointers[0] = (List<ShardPointer>) gson.fromJson(response, new TypeToken<List<ShardPointer>>() {}.getType());
                                        shardPointersLoadedLatch.countDown();
                                    }
                                });
                            }
                        };

                        loadShardPointersThread.setName("loadShardPointers");
                        loadShardPointersThread.start();
                    }
                });

                try {
                    shardPointersLoadedLatch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                for (ShardPointer shardPointer : shardPointers[0]) {
                    File shardFile = null;
                    try {
                        shardFile = File.createTempFile(bucketEntryId + "_", "_shard_" + shardPointer.getIndex(), context.getExternalCacheDir());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    shardPointer.setPath(shardFile.getPath());
                    shardPointer.setTransferId(queuedDownload.getTransferId());
                    shardPointer.setTransferStatus(ShardStatus.WAITING);

                    queuedDownload.addShard(shardPointer);
                }


                ShardDownloadingManager.getInstance(cryptoUtils).addFileToQueue(queuedDownload, allFilesTransferredListener);

                queuedDownload.waitForAllShardsToBeTransferred();

                System.out.println(" ======== ALL SHARDS DOWNLOADED ========");

                FileMuxingManager.getInstance().addToMuxQueue(new FileMuxerRunnable(queuedDownload, cryptoUtils));

                try {
                    queuedDownload.getMuxingCompletedLatch().await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                queuedDownload.getFileTransferListener().onFileTransferred(queuedDownload);

            }
        }.start();

    }

    /**
     * Creates a new file staging frame.
     *
     * @return the created frame.
     */
    protected void createFrame(SharedPreferences sharedPreferences, Response.Listener<JSONObject> responseListener) throws JSONException {
        String gsonFrame = gson.toJson(new Frame());
        sendPostRequest(storjApiFrames, gsonFrame, responseListener);
    }

    protected void createBucketEntry(QueuedUpload qu){
        createBucketEntry(qu.getFrame(), qu.getUnencryptedFile(), qu.getEncryptedFile(), qu.getBucketId(), qu.getBucketEntryCreatedLatch());
    }

    protected void createBucketEntry(Frame frame, File inputFile, File encryptedFile, String bucketId, CountDownLatch bucketEntryCreatedLatch){
        BucketEntry bucketEntry = new BucketEntry();

        bucketEntry.setMimetype(MimeTypeGetter.getMimeType(inputFile.getPath()));
        bucketEntry.setFilename(inputFile.getName());
        bucketEntry.setFrame(frame.getId());
        bucketEntry.setSize(encryptedFile.length());

//        HMAC hmac = new HMAC();
//        hmac.setType("sha512");
//        hmac.setValue(CryptoUtils.bytesToHex(Hashing.sha512().hashBytes(shards.get(0).getHash().getBytes()).asBytes()));
//        hmac.setValue(CryptoUtils.bytesToHex(Hashing.sha512().hashBytes(CryptoUtils.hexToBytes(shards.get(0).getHash())).asBytes()));
//        bucketEntry.setHmac(hmac);


        bucketEntry.setFrame(frame.getId());

        // store the bucket entry.
        this.storeFile(bucketId, bucketEntry, bucketEntryCreatedLatch);
    }

    public void storeFile(String bucketId, final BucketEntry bucketEntry, final CountDownLatch bucketEntryCreatedLatch){
        String requestUrl =  storjApiBuckets + File.separator + bucketId + File.separator + "files";

        try {
            sendPostRequest(requestUrl, gson.toJson(bucketEntry, BucketEntry.class), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("File Upload", "File '" + bucketEntry.getFilename() + "' uploaded / BucketEntry created [Thread: " + Thread.currentThread().getName() + ", Thread id: " + Thread.currentThread().getId() + "]");
                    bucketEntryCreatedLatch.countDown();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load a token for the specified bucket and operation.
     * Token can be accessed through the Response Listener
     *
     * @param bucketId
     *            the ID of the bucket to create a token for.
     * @param operation
     *            The operation to create a token for.
     * @param responseListener
     *            Defining what to do after the result is received.
     */
    public void loadTokenForBucket(String bucketId, Operation operation, Response.Listener<JSONObject> responseListener) {

        String requestUrl = storjApiBuckets + "/" + bucketId + "/" + "tokens";
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("operation", operation.toString());
        String postBodyJson = gson.toJson(postBody);
        try {
            sendPostRequest(requestUrl, postBodyJson, responseListener);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /**
     * Load file pointers for a file on the storj network.
     * ShardPointers can be accessed through the Response Listener
     *
     * @param bucketId
     *            The ID of the bucket the file belongs in.
     * @param fileId
     *            The ID of the file.
     * @param xToken
     *            The access token.
     * @param responseListener
     *            Defining what to do after the result is received.
     */
    public void loadShardPointers(String bucketId, String fileId, String xToken, Response.Listener<String> responseListener) {
        String requestUrl = storjApiBuckets + "/" + bucketId + "/files/" + fileId;

        // todo remove
//        // Add URL params.
//        Map<String, String> urlParams = new HashMap<String, String>();
//        urlParams.put("skip", "0");
//        urlParams.put("limit", "10");
//        requestUrl += MapQueryEncoderUtils.urlEncodeUTF8(urlParams);

        Map<String, String> mHeaders = new ArrayMap<String, String>();
        mHeaders.put("x-token", xToken);

        sendGetRequest(requestUrl, responseListener, mHeaders);
    }

    public static String getStorjApiExchangeReports() {
        return storjApiExchangeReports;
    }

    public boolean isTransferring(){
        for(QueuedTransfer queuedTransfer : transferList) {
            // TODO remove
            System.out.println(queuedTransfer.getUnencryptedFile().getName() + ": " + queuedTransfer.getStatus());

            switch(queuedTransfer.getStatus()){
                case WAITING:
                case TRANSFERRING:
                case ENCRYPTING:
                case SHARDING:
                case DECRYPTING:
                case DESHARDING:
                case CREATING_BUCKET_ENTRY:

                    return true;
            }

        }
        return false;
    }

}

