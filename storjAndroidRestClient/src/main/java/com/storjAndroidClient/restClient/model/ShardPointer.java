package com.storjAndroidClient.restClient.model;

import com.storjAndroidClient.restClient.backgroundServices.templateClasses.Shard;

/**
 * Created by Gabriel on 13.10.2016.
 */

public class ShardPointer extends Shard {

    String token;
    Operation operation;
    Contact farmer;

    public Contact getFarmer() {
        return farmer;
    }

    public void setFarmer(Contact farmer) {
        this.farmer = farmer;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    @Override
    public String toString() {
        return "ShardPointer{" +
                super.toString() +
                ", token='" + token + '\'' +
                ", operation=" + operation +
                '}';
    }
}
