package com.storjAndroidClient.restClient.model;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Created by Gabriel Comte on 12.10.2016.
 */

public class Token {

    String bucket;
    String expires;
    Operation operation;
    String token;
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getExpires() {
        return expires;
    }

    public DateTime getExpiresAsDateTime(){
        // Date will be converted to date in the user's time zone.
        return ISODateTimeFormat.dateTime().parseDateTime(this.getExpires()); // see http://stackoverflow.com/questions/17725168/how-to-parse-joda-time-of-this-format
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Token{" +
                "bucket='" + bucket + '\'' +
                ", expires='" + expires + '\'' +
                ", operation=" + operation +
                ", token='" + token + '\'' +
                '}';
    }
}
