package com.storjAndroidClient.restClient.model;

/**
 * Created by Gabriel Comte on 13.10.2016.
 */

public enum BucketStatus {
    ACTIVE, INACTIVE;
}
