package com.storjAndroidClient.restClient.model;

/**
 * Created by Gabriel Comte on 12.10.2016.
 */

public enum Operation {
    PUSH, PULL;
}