package com.storjAndroidClient.restClient.model;

import com.storjAndroidClient.restClient.backgroundServices.templateClasses.Shard;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.ShardStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Gabriel Comte on 22.10.2016.
 */

public class UploadableShard extends Shard {

    private List<String> tree = new ArrayList<String>();
    private List<String> challenges = new ArrayList<String>();

    // should be excluded by GSON => transient (http://stackoverflow.com/questions/4802887/gson-how-to-exclude-specific-fields-from-serialization-without-annotations)
    transient private ShardUploadParameters uploadParameters;
    transient private CountDownLatch addedToFrameLatch;

    public UploadableShard() {
        this.addedToFrameLatch = new CountDownLatch(1);
    }

    public void setTransferStatus(ShardStatus transferStatus){
        super.setTransferStatus(transferStatus);

        if(transferStatus == ShardStatus.CANCELED || transferStatus == ShardStatus.FAILED) {
            // this latch need to be released, if not the algorithm is stuck
            if (this.addedToFrameLatch.getCount() > 0) {
                this.addedToFrameLatch.countDown();
            }
        }
    }

    public CountDownLatch getAddedToFrameLatch() {
        return addedToFrameLatch;
    }

    public List<String> getTree() {
        return tree;
    }

    public void setTree(List<String> tree) {
        this.tree = tree;
    }

    public List<String> getChallenges() {
        return challenges;
    }

    public void setChallenges(List<String> challenges) {
        this.challenges = challenges;
    }

    public ShardUploadParameters getUploadParameters() {
        return uploadParameters;
    }

    public void setUploadParameters(ShardUploadParameters uploadParameters) {
        this.uploadParameters = uploadParameters;
    }

    @Override
    public String toString() {
        return "UploadableShard{" + super.toString() +
                ", tree=" + tree +
                ", challenges=" + challenges +
                '}';
    }
}
