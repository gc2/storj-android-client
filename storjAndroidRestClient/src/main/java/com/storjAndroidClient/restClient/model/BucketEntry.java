package com.storjAndroidClient.restClient.model;

import com.storjAndroidClient.restClient.helperClasses.FileSizeUtils;

/**
 * Created by Gabriel Comte on 10.10.2016.
 */

public class BucketEntry {
    protected String id;
    protected String hash;
    protected String bucket;
    protected String mimetype;
    protected String filename;
    protected long size;
    protected String frame;
    protected String name;
    protected String renewal;
    protected HMAC hmac;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRenewal() {
        return renewal;
    }

    public void setRenewal(String renewal) {
        this.renewal = renewal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrame() {
        return frame;
    }

    public void setFrame(String frame) {
        this.frame = frame;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public HMAC getHmac() {
        return hmac;
    }

    public void setHmac(HMAC hmac) {
        this.hmac = hmac;
    }

    @Override
    public String toString() {
        return "BucketEntry{" +
                "id='" + id + '\'' +
                ", hash='" + hash + '\'' +
                ", bucket='" + bucket + '\'' +
                ", mimetype='" + mimetype + '\'' +
                ", filename='" + filename + '\'' +
                ", size=" + size +
                ", frame='" + frame + '\'' +
                ", name='" + name + '\'' +
                ", renewal='" + renewal + '\'' +
                ", HMAC='" + hmac + '\'' +
                '}';
    }

    public String getHumanReadableSize(){
        return FileSizeUtils.humanReadableByteCount(this.getSize(), true);
    }
}
