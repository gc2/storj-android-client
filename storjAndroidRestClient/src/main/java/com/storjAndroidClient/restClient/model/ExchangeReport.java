package com.storjAndroidClient.restClient.model;

/**
 * Created by Gabriel Comte on 17.2.2017.
 */

public class ExchangeReport {

    protected String dataHash; // The Shard's Hash
    protected String reporterId; // User's PUBLICKEY (In hex, as usual)
    protected String farmerId; // This is the farmer's hash id that you're uploading or downloading from.
    protected String clientId; // Also the User's PUBLICKEY (In hex, as usual)

    protected int exchangeStart; // Unix Time (32bit integer) before the transfer has started.
    protected int exchangeEnd; // Unix Time (32bit integer) after the transfer has ended or failed.

    protected int exchangeResultCode; // Either 1000 for success or 1100 for a failure
    protected ExchangeReportMessage exchangeResultMessage; // One of the following strings:   SHARD_UPLOADED | DOWNLOAD_ERROR | MIRROR_SUCCESS

    public String getDataHash() {
        return dataHash;
    }

    public void setDataHash(String dataHash) {
        this.dataHash = dataHash;
    }

    public String getReporterId() {
        return reporterId;
    }

    public void setReporterId(String reporterId) {
        this.reporterId = reporterId;
    }

    public String getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public int getExchangeStart() {
        return exchangeStart;
    }

    public void setExchangeStart(int exchangeStart) {
        this.exchangeStart = exchangeStart;
    }

    public int getExchangeEnd() {
        return exchangeEnd;
    }

    public void setExchangeEnd(int exchangeEnd) {
        this.exchangeEnd = exchangeEnd;
    }

    public int getExchangeResultCode() {
        return exchangeResultCode;
    }

    public void setExchangeResultCode(int exchangeResultCode) {
        this.exchangeResultCode = exchangeResultCode;
    }

    public ExchangeReportMessage getExchangeResultMessage() {
        return exchangeResultMessage;
    }

    public void setExchangeResultMessage(ExchangeReportMessage exchangeResultMessage) {
        this.exchangeResultMessage = exchangeResultMessage;
    }

    @Override
    public String toString() {
        return "ExchangeReport{" +
                "dataHash='" + dataHash + '\'' +
                ", reporterId='" + reporterId + '\'' +
                ", farmerId='" + farmerId + '\'' +
                ", clientId='" + clientId + '\'' +
                ", exchangeStart=" + exchangeStart +
                ", exchangeEnd=" + exchangeEnd +
                ", exchangeResultCode=" + exchangeResultCode +
                ", exchangeResultMessage=" + exchangeResultMessage +
                '}';
    }
}
