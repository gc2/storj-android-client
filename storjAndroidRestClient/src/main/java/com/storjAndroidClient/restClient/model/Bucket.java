package com.storjAndroidClient.restClient.model;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.util.ArrayList;
import java.util.List;

import static com.storjAndroidClient.restClient.model.BucketStatus.ACTIVE;
import static com.storjAndroidClient.restClient.model.BucketStatus.INACTIVE;

/**
 * Created by Gabriel Comte on 04. DECEMBER 2016
 */
public class Bucket {

    //  According to https://github.com/Storj/service-storage-models/blob/master/lib/models/bucket.js a bucket can have the status active as well as inactive
    public static final String STATUS_INACTIVE_KEYWORD = "Inactive";
    public static final String STATUS_ACTIVE_KEYWORD = "Active";

    protected int storage;
    protected int transfer;
    protected String status;
    protected List<String> pubkeys;
    protected String user;
    protected String name;
    protected String created;
    protected String id;

    // these two attributes were added by Gabriel Comte, on 11. February 2017.
    // Gabriel Comte guesses, that both these attributes were added to the Buckets as part of the feature "public bucket"
    protected String encryptionKey;
    protected List<String> publicPermissions;

    public int getStorage() {
        return storage;
    }

    public void setStorage(int storage) {
        this.storage = storage;
    }

    public int getTransfer() {
        return transfer;
    }

    public void setTransfer(int transfer) {
        this.transfer = transfer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getPubkeys() {
        return pubkeys;
    }

    public void setPubkeys(List<String> pubkeys) {
        this.pubkeys = pubkeys;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEncryptionKey() {
        return encryptionKey;
    }

    public void setEncryptionKey(String encryptionKey) {
        this.encryptionKey = encryptionKey;
    }

    public List<String> getPublicPermissions() {
        return publicPermissions;
    }

    public void setPublicPermissions(List<String> publicPermissions) {
        this.publicPermissions = publicPermissions;
    }

    public void setPublicPermissionsAsEnums(List<PublicBucketPermissions> publicPermissions) {
        List<String> stringPermissions = new ArrayList<String>();

        for(PublicBucketPermissions enumPermission : publicPermissions){
            stringPermissions.add(enumPermission.toString());
        }

        setPublicPermissions(stringPermissions);
    }

    public List<PublicBucketPermissions> getPublicPermissionsAsEnums() {
        List<PublicBucketPermissions> enumPermissions = new ArrayList<PublicBucketPermissions>();

        for(String stringPermission : this.getPublicPermissions()){
            enumPermissions.add(PublicBucketPermissions.valueOf(stringPermission));
        }

        return enumPermissions;
    }

    public void setStatusByKey(BucketStatus status) {
        switch (status) {
            case INACTIVE:
            default:
                this.setStatus(Bucket.STATUS_INACTIVE_KEYWORD);
                break;
            case ACTIVE:
                this.setStatus(Bucket.STATUS_ACTIVE_KEYWORD);
                break;
        }
    }

    public BucketStatus getStatusKey() {

        BucketStatus status;

        switch (this.getStatus()) {
            case Bucket.STATUS_INACTIVE_KEYWORD:
            default:
                status = INACTIVE;
                break;
            case Bucket.STATUS_ACTIVE_KEYWORD:
                status = ACTIVE;
                break;
        }
        return status;
    }

    public DateTime getCreatedAsDateTime(){
        // Date will be converted to date in the user's time zone.
        return ISODateTimeFormat.dateTime().parseDateTime(this.getCreated()); // see http://stackoverflow.com/questions/17725168/how-to-parse-joda-time-of-this-format
    }

    public void setCreatedAsDateTime(DateTime dateTime){
        this.setCreated(dateTime.toString()); // see http://stackoverflow.com/questions/14752782/jodatime-datetime-iso8601-gmt-date-format
    }

    @Override
    public String toString() {

        return "Bucket{" +
                "storage=" + storage +
                ", transfer=" + transfer +
                ", status='" + status + '\'' +
                ", pubkeys=" + pubkeys +
                ", user='" + user + '\'' +
                ", name='" + name + '\'' +
                ", created='" + created + '\'' +
                ", id='" + id + '\'' +
                ", encryptionKey='" + encryptionKey + '\'' +
                ", publicPermissions=" + publicPermissions +
                '}';
    }
}
