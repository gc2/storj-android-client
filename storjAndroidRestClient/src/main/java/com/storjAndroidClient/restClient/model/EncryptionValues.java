package com.storjAndroidClient.restClient.model;

import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;

/**
 * Created by Gabriel Comte on 12.10.2016.
 */

public class EncryptionValues {

    protected byte[] encryptionKey;
    protected byte[] initializationVector;

    public EncryptionValues(byte[] encryptionKey, byte[] initializationVector) {
        this.setEncryptionKey(encryptionKey);
        this.setInitializationVector(initializationVector);
    }
    public EncryptionValues(String encryptionKey, byte[] initializationVector) {
        this.setEncryptionKey(encryptionKey);
        this.setInitializationVector(initializationVector);
    }
    public EncryptionValues(byte[] encryptionKey, String initializationVector) {
        this.setEncryptionKey(encryptionKey);
        this.setInitializationVector(initializationVector);
    }
    public EncryptionValues(String encryptionKey, String initializationVector) {
        this.setEncryptionKey(encryptionKey);
        this.setInitializationVector(initializationVector);
    }

    public byte[] getEncryptionKey() {
        return encryptionKey;
    }

    public void setEncryptionKey(byte[] encryptionKey) {
        this.encryptionKey = encryptionKey;
    }

    public byte[] getInitializationVector() {
        return initializationVector;
    }

    public void setInitializationVector(byte[] initializationVector) {
        this.initializationVector = initializationVector;
    }


    // supporting hexadecimal representation

    public String getEncryptionKeyHex() {
        return CryptoUtils.bytesToHex(encryptionKey);
    }

    public void setEncryptionKey(String encryptionKey) {
        this.encryptionKey = CryptoUtils.hexToBytes(encryptionKey);
    }

    public String getInitializationVectorHex() {
        return CryptoUtils.bytesToHex(initializationVector);
    }

    public void setInitializationVector(String initializationVector) {
        this.initializationVector = CryptoUtils.hexToBytes(initializationVector);
    }

    @Override
    public String toString() {
        return "EncryptionValues{" +
                "encryptionKey=" + getEncryptionKeyHex() +
                ", initializationVector=" + getInitializationVectorHex() +
                '}';
    }

}
