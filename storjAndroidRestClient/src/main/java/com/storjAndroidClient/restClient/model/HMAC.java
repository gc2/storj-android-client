package com.storjAndroidClient.restClient.model;

/**
 * Created by Gabriel Comte on 15.4.2017.
 */

public class HMAC {

    private String type;
    private String value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "HMAC{" +
                "type='" + type + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
