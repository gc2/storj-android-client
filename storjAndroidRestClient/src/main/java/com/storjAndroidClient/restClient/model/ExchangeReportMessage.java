package com.storjAndroidClient.restClient.model;

/**
 * Created by Gabriel Comte on 17.2.2017.
 *
 * note by rocketchat user Knowledge:
 * I think Mirror_Success is probably what the Farmer returns when it creates a mirror, so you wouldn't normally pass this. There doesn't appear
 * to be a message for UPLOAD_ERROR. Probably because they aren't charging for uploads, so they don't care
 */

public enum ExchangeReportMessage {
    SHARD_UPLOADED, DOWNLOAD_ERROR, MIRROR_SUCCESS
}
