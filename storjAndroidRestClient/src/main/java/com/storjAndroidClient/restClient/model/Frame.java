package com.storjAndroidClient.restClient.model;

import java.util.List;

/**
 * Represents a file staging frame.
 * Created by Gabriel Comte on 22.10.2016.
 */

public class Frame {
    String user;
    String created;
    String id;
    List<String> shards;
    int size;
    boolean locked;

    //todo remove ( plus getter and setter)
    // used by the android implementation, but NOT part of what is a Frame on the Bridge
    // should be excluded by GSON => transient (http://stackoverflow.com/questions/4802887/gson-how-to-exclude-specific-fields-from-serialization-without-annotations)
//    transient String uploadId;
//    transient String accordingEncryptedFile;
//    transient String accordingFileName;


//    public String getUploadId() {
//        return uploadId;
//    }
//
//    public void setUploadId(String uploadId) {
//        this.uploadId = uploadId;
//    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getShards() {
        return shards;
    }

    public void setShards(List<String> shards) {
        this.shards = shards;
    }

//    public String getAccordingEncryptedFile() {
//        return accordingEncryptedFile;
//    }
//
//    public void setAccordingEncryptedFile(String accordingEncryptedFile) {
//        this.accordingEncryptedFile = accordingEncryptedFile;
//    }
//
//    public String getAccordingFileName() {
//        return accordingFileName;
//    }
//
//    public void setAccordingFileName(String accordingFileName) {
//        this.accordingFileName = accordingFileName;
//    }

    @Override
    public String toString() {
        return "Frame{" +
                "created='" + created + '\'' +
                ", id='" + id + '\'' +
                ", shards=" + shards +
                ", user='" + user + '\'' +
                ", size=" + size +
                ", locked=" + locked +
//                ", uploadId=" + uploadId +
//                ", accordingFileName=" + accordingFileName +
//                ", accordingEncryptedFile=" + accordingEncryptedFile +
                '}';
    }
}
