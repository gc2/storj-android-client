package com.storjAndroidClient.restClient.model;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Created by Gabriel Comte on 13.10.2016.
 */

public class Contact {
    private String userAgent;
    private String protocol;
    private String address;
    private int port;
    private String lastSeen; // format: "2017-05-25T18:38:35.207Z"
    private String nodeID;

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getNodeID() {
        return nodeID;
    }

    public void setNodeID(String nodeId) {
        this.nodeID = nodeId;
    }


    public DateTime getLastSeenAsDateTime(){
        // Date will be converted to date in the user's time zone.
        return ISODateTimeFormat.dateTime().parseDateTime(this.getLastSeen()); // see http://stackoverflow.com/questions/17725168/how-to-parse-joda-time-of-this-format
    }

    public void setLastSeenAsDateTime(DateTime dateTime){
        this.setLastSeen(dateTime.toString()); // see http://stackoverflow.com/questions/14752782/jodatime-datetime-iso8601-gmt-date-format
    }


    @Override
    public String toString() {
        return "Contact{" +
                "protocol='" + protocol + '\'' +
                ", address='" + address + '\'' +
                ", port=" + port +
                ", userAgent='" + userAgent + '\'' +
                ", lastSeen=" + lastSeen +
                ", nodeID='" + nodeID + '\'' +
                '}';
    }
}
