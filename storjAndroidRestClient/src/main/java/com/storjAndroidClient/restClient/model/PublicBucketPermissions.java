package com.storjAndroidClient.restClient.model;

/**
 * Created by Gabriel Comte on 04.03.2017.
 */

public enum PublicBucketPermissions {
    PUSH, PULL;
}
