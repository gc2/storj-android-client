package com.storjAndroidClient.restClient.auth;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Gabriel Comte
 *
 * Authenticates
 *
 */
public class BasicAuthTypeStringRequest extends StringRequest {
    private HashMap<String, String> headerParams;

    public BasicAuthTypeStringRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener, String username, String password) {
        super(method, url, listener, errorListener);
        String authorizationHeaderValue = "Basic " + base64Encode(username + ":" + sha256Encrypt(password));
        headerParams = new HashMap<String, String>();
        headerParams.put("Authorization", authorizationHeaderValue);
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headerParams;
    }


    // WE NEED AT LEAST KIT KAT (ANDROID 4.4) FOR THIS!
    private String sha256Encrypt(String input){
        return Hashing.sha256()
                .hashString(input, StandardCharsets.UTF_8)
                .toString();
    }

    private String base64Encode(String input){
        return BaseEncoding.base64().encode(input.getBytes(StandardCharsets.UTF_8));
    }

    public void addToHeader(Map<String, String> mHeader){
        headerParams.putAll(mHeader);
    }
}
