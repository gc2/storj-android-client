package com.storjAndroidClient.restClient.auth;

import android.content.Context;

import java.io.Serializable;

/**
 * Created by Gabriel Comte on 18.1.2017.
 */

public interface AuthenticationFailedListener extends Serializable {
    public void onFailedAuthentication(Context context);
}
