package com.storjAndroidClient.restClient.auth;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Gabriel Comte
 *
 * Authenticates the by signing the request with the private key
 * nonce must be different for every request
 */
public class SignatureAuthTypeStringRequest extends StringRequest {
    private HashMap<String, String> headerParams;

    public SignatureAuthTypeStringRequest(int method, String baseUrl, String url, String nonce, CryptoUtils cryptoUtils, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);

        headerParams = new HashMap<String, String>();

        // for example "/buckets/d3c74b23599e82e8c68d5917/files"
        String urlPath = url.substring(baseUrl.length());
        if(url.indexOf("?") > 0){
            urlPath = urlPath.substring(0, urlPath.indexOf("?"));
        }

        // for example "GET\n/buckets/d3c74b23599e82e8c68d5917/files\n__nonce=c5a73455-d017-4597-b24e-995ae1592de8"
        String signatureData = getMethodString(method) + "\n" + urlPath + "\n__nonce=" + nonce;

        String signature = signData(cryptoUtils.loadPrivatKey(), signatureData);

        headerParams.put("x-signature", signature);
        headerParams.put("x-pubkey", cryptoUtils.loadPublicKey());
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headerParams;
    }

    public void addToHeader(Map<String, String> mHeader){
        headerParams.putAll(mHeader);
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }


    public static String signData(PrivateKey privateKey, String data) {
        try {
            Signature sig = Signature.getInstance("SHA256withECDSA");
            sig.initSign(privateKey);
            sig.update(data.getBytes());
            byte[] signature = sig.sign();

            return CryptoUtils.bytesToHex(signature);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getMethodString(int method){
        switch(method) {
            default:
            case Method.DEPRECATED_GET_OR_POST:
                return "DEPRECATED_GET_OR_POST";
            case Method.GET:
                return "GET";
            case Method.POST:
                return "POST";
            case Method.PUT:
                return "PUT";
            case Method.DELETE:
                return "DELETE";
            case Method.HEAD:
                return "HEAD";
            case Method.OPTIONS:
                return "OPTIONS";
            case Method.TRACE:
                return "HEAD";
            case Method.PATCH:
                return "PATCH";
        }
    }
}
