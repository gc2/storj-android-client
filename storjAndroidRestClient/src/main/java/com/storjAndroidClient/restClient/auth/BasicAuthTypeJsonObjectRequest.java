package com.storjAndroidClient.restClient.auth;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Gabriel Comte
 *
 * Authenticates
 *
 */
public class BasicAuthTypeJsonObjectRequest extends JsonObjectRequest {
    private String authorizationHeaderValue;

    public BasicAuthTypeJsonObjectRequest(int method, String url, JSONObject jsonObject, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener, String username, String password) {
        super(method, url, jsonObject, listener, errorListener);
        authorizationHeaderValue = "Basic " + base64Encode(username + ":" + sha256Encrypt(password));
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> params = new HashMap<String, String>();

        params.put("Authorization", authorizationHeaderValue);
        return params;
    }


    // WE NEED AT LEAST KIT KAT (ANDROID 4.4) FOR THIS!
    private String sha256Encrypt(String input){
        return Hashing.sha256()
                .hashString(input, StandardCharsets.UTF_8)
                .toString();
    }

    private String base64Encode(String input){
        return BaseEncoding.base64().encode(input.getBytes(StandardCharsets.UTF_8));
    }
}
