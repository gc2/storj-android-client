package com.storjAndroidClient.restClient.auth;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;

import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Gabriel Comte
 *
 * Authenticates
 *
 */
public class SignatureAuthTypeJsonObjectRequest extends JsonObjectRequest {
    private HashMap<String, String> headerParams;

    public SignatureAuthTypeJsonObjectRequest(int method, String baseUrl, String url, CryptoUtils cryptoUtils, JSONObject jsonObject, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonObject, listener, errorListener);

        headerParams = new HashMap<String, String>();

        // for example "/buckets/d3c74b23599e82e8c68d5917/files"
        String urlPath = url.substring(baseUrl.length());
        if(url.indexOf("?") > 0){
            urlPath = urlPath.substring(0, urlPath.indexOf("?"));
        }

        // for example:
        // PATCH\n/buckets\{"name":"xyz","storage":0,"transfer":0,"__nonce":"4bbc5612-5ed4-4442-bb16-ec5a3492c50e"}
        String signatureData = getMethodString(method) + "\n" + urlPath + "\n" + jsonObject.toString();

        String signature = signData(cryptoUtils.loadPrivatKey(), signatureData);

        headerParams.put("x-signature", signature);
        headerParams.put("x-pubkey", cryptoUtils.loadPublicKey());
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headerParams;
    }

    public void addToHeader(Map<String, String> mHeader){
        headerParams.putAll(mHeader);
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }


    public static String signData(PrivateKey privateKey, String data) {
        try {
            Signature sig = Signature.getInstance("SHA256withECDSA");
            sig.initSign(privateKey);
            sig.update(data.getBytes());
            byte[] signature = sig.sign();

            return CryptoUtils.bytesToHex(signature);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getMethodString(int method){
        switch(method) {
            default:
            case Method.DEPRECATED_GET_OR_POST:
                return "DEPRECATED_GET_OR_POST";
            case Method.GET:
                return "GET";
            case Method.POST:
                return "POST";
            case Method.PUT:
                return "PUT";
            case Method.DELETE:
                return "DELETE";
            case Method.HEAD:
                return "HEAD";
            case Method.OPTIONS:
                return "OPTIONS";
            case Method.TRACE:
                return "HEAD";
            case Method.PATCH:
                return "PATCH";
        }
    }

}
