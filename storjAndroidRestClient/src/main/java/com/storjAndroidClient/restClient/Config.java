package com.storjAndroidClient.restClient;

/**
 * Created by Gabriel Comte on 28.2.2017.
 */

public class Config {

    // shared preferences files
    public static final String SHARED_PREF_ENCRYPTION_VARS = "storjEncryptionVariables";

    // sharding
    // files smaller than 2 MB are not being sharded.
    public static final int MINIMAL_SHARD_SIZE = 2;
    // there are never more than 8 shards per file
    public static final int MAXIMAL_SHARD_AMOUNT = 8;

    public static final int NUMBER_OF_CHALLENGES = 8;

    public static final int ADD_SHARD_TO_FRAME_RETRIES = 8;

}
