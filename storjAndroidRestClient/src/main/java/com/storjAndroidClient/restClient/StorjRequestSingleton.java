package com.storjAndroidClient.restClient;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Gabriel Comte on 28.09.2016.
 */
public class StorjRequestSingleton {
    private static StorjRequestSingleton mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    private StorjRequestSingleton(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized StorjRequestSingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new StorjRequestSingleton(context);
        }

        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

}
