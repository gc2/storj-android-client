package com.storjAndroidClient.restClient.helperClasses;

import android.webkit.MimeTypeMap;

/**
 * Created by Gabriel Comte on 24.10.2016.
 */

public class MimeTypeGetter {

    // copied from http://stackoverflow.com/questions/8589645/how-to-determine-mime-type-of-file-in-android
    // url = file path or whatever suitable URL you want.
    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url.replaceAll("\\s+","")).toLowerCase();
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }
}
