package com.storjAndroidClient.restClient.backgroundServices.templateClasses;

import android.util.Log;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.storjAndroidClient.restClient.StorjRestClient;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;
import com.storjAndroidClient.restClient.model.ExchangeReport;
import com.storjAndroidClient.restClient.model.ExchangeReportMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Gabriel Comte on 17.2.2017.
 */

public class ExchangeReportSender {

    public static void sendExchangeReport(int shardTransferStart, int shardTransferEnd, Shard shard, CryptoUtils cryptoUtils, String farmerId, int result, ExchangeReportMessage reportMessage){

        Gson gson = new Gson();
        ExchangeReport exchangeReport = new ExchangeReport();

        exchangeReport.setClientId(cryptoUtils.loadPublicKey());
        exchangeReport.setReporterId(cryptoUtils.loadPublicKey());
        exchangeReport.setFarmerId(farmerId);
        exchangeReport.setDataHash(shard.getHash());

        exchangeReport.setExchangeStart(shardTransferStart);
        exchangeReport.setExchangeEnd(shardTransferEnd);

        exchangeReport.setExchangeResultCode(result);
        exchangeReport.setExchangeResultMessage(reportMessage);

        try {
            StorjRestClient.getInstance().sendPostRequest(StorjRestClient.getStorjApiExchangeReports(), gson.toJson(exchangeReport, ExchangeReport.class), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Exchange Report", "Response: " + response);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
