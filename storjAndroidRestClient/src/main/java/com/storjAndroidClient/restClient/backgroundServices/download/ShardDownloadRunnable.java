package com.storjAndroidClient.restClient.backgroundServices.download;

import android.os.Process;

import com.storjAndroidClient.restClient.backgroundServices.templateClasses.ExchangeReportSender;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.Shard;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.ShardStatus;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.TransferStatus;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;
import com.storjAndroidClient.restClient.fileHandling.fileTransfer.HttpShardTransfer;
import com.storjAndroidClient.restClient.model.ExchangeReportMessage;
import com.storjAndroidClient.restClient.model.ExchangeReportResult;
import com.storjAndroidClient.restClient.model.ShardPointer;

/**
 * Created by Gabriel Comte on 17.5.2017.
 *
 * Downloads a shard
 * Designed to run on an its own Thread.
 */

public class ShardDownloadRunnable implements Runnable {

    private Thread thisThread;

    private QueuedDownload queuedDownload;
    private ShardPointer shardPointer;
    private static CryptoUtils cryptoUtils;

    public ShardDownloadRunnable(QueuedDownload queuedDownload, ShardPointer shardPointer, CryptoUtils cryptoUtils) {

        this.queuedDownload = queuedDownload;
        this.shardPointer = shardPointer;
        ShardDownloadRunnable.cryptoUtils = cryptoUtils;
    }

    public boolean belongsToDownload(String uploadId){
        return uploadId.equals(queuedDownload.getTransferId());
    }

    @Override
    public void run() {

        thisThread = Thread.currentThread();

        // give the thread background priority. With this priority, it is still more prioritized than the sharding process.
        // The idea behind making it more favourable, is that as soon as a file is demuxed, the highest priority is to upload it, NOT to demuxed any next file
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

        if (Thread.interrupted()) {
            return;
        }

        // ===================== STEP 1: DOWNLOAD SHARD FROM FARMER ===================== //

        if(queuedDownload.getStatus() == TransferStatus.WAITING_FOR_TRANSFER){
            queuedDownload.setStatus(TransferStatus.TRANSFERRING);
        }

        int shardDownloadStart = Math.round(System.currentTimeMillis() / 1000L); // unix timestamp
        int shardDownloadEnd;
        boolean downloadSuccessful = false;

        try {
            System.out.println("bevor receive shard");
            HttpShardTransfer.receiveShard(shardPointer);
            System.out.println("set TRANSFERRED");
            shardPointer.setTransferStatus(ShardStatus.TRANSFERRED);
            downloadSuccessful = true;

            // if all shards have the status TRANSFERRED, then the file is downloaded and the file may be multiplexed (muxing)
            boolean allShardsTransferred = true;
            for (Shard shard : queuedDownload.getShards()) {
                if (shard.getTransferStatus() != ShardStatus.TRANSFERRED) {
                    allShardsTransferred = false;
                }
            }
            if (allShardsTransferred) {
                queuedDownload.setStatus(TransferStatus.DESHARDING);
                queuedDownload.getAllShardsTransferLatch().countDown();
            }

        } catch (Exception e) {
            e.printStackTrace();
            shardPointer.setTransferStatus(ShardStatus.FAILED);
        }finally {

            // ===================== STEP 2: SEND EXCHANGE REPORT ===================== //

            shardDownloadEnd = Math.round(System.currentTimeMillis() / 1000L); // unix timestamp

            if(downloadSuccessful) {
                System.out.println("Shard-download successful");
                // so far, we don't send any exchange reports for successful downloads
                ExchangeReportSender.sendExchangeReport(shardDownloadStart, shardDownloadEnd, shardPointer, cryptoUtils, shardPointer.getFarmer().getNodeID(), ExchangeReportResult.SUCCESS, ExchangeReportMessage.DOWNLOAD_ERROR);
            }else{
                System.out.println("Shard-download unsuccessful");
                queuedDownload.setStatus(TransferStatus.FAILED);

                // send exchange report [upload was unsuccessful] - it is not clear yet whether this exchange must be sent or only the one on success (Stand 17.02.2017)
                ExchangeReportSender.sendExchangeReport(shardDownloadStart, shardDownloadEnd, shardPointer, cryptoUtils, shardPointer.getFarmer().getNodeID(), ExchangeReportResult.FAILURE, ExchangeReportMessage.SHARD_UPLOADED);

                // download failed, cleanup everything
                queuedDownload.getFileTransferListener().onTransferFailed(queuedDownload);
            }


            // ===================== STEP 3: RELEASE SHARD DOWNLOAD LATCH ===================== //
//            queuedDownload.

            System.out.println("check this out now!!");

        }
    }

    public Thread getThread(){
        return thisThread;
    }


}
