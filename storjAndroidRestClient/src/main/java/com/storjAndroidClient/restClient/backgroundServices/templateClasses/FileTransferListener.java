package com.storjAndroidClient.restClient.backgroundServices.templateClasses;

import java.io.Serializable;

/**
 * Created by Gabriel Comte on 06.11.2016.
 */

public interface FileTransferListener extends Serializable {
    void onFileTransferred(QueuedTransfer queuedTransfer);
    void onTransferFailed(QueuedTransfer queuedTransfer);
}
