package com.storjAndroidClient.restClient.backgroundServices.download;

import android.os.Process;
import android.util.Log;

import com.storjAndroidClient.restClient.backgroundServices.templateClasses.Shard;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.TransferStatus;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;
import com.storjAndroidClient.restClient.sharding.ShardingUtils;

import java.io.File;

/**
 * Created by Gabriel Comte on 23.05.2017.
 *
 * Encrypts a file and then it shards it
 * Designed to run on its own Thread.
 */

public class FileMuxerRunnable implements Runnable {

    private Thread thisThread;
    private QueuedDownload queuedDownload;
    private CryptoUtils cryptoUtils;

    public FileMuxerRunnable(QueuedDownload queuedDownload, CryptoUtils cryptoUtils) {

        this.queuedDownload = queuedDownload;
        this.cryptoUtils = cryptoUtils;
    }

    public boolean belongsToDownload(String downloadId){
        return downloadId.equals(queuedDownload.getTransferId());
    }

    @Override
    public void run() {

        // give the thread background priority but make it less prioritized than the uploading process.
        // The idea behind making it less favourable, is that as soon as a file is demuxed, the highest priority is to upload it, NOT to demuxed the next file
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND + Process.THREAD_PRIORITY_LESS_FAVORABLE);

        // ===================== STEP 1: PUTTING SHARDS TOGETHER TO ONE FILE ===================== //
        if (Thread.interrupted()) {
            return;
        }

        queuedDownload.setStatus(TransferStatus.DESHARDING);

        try {
            ShardingUtils.pieceTogetherShards(queuedDownload.getShards(), queuedDownload.getEncryptedFile());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // ===================== STEP 2: DECRYPTING FILE ===================== //

        if (Thread.interrupted()) {
            return;
        }

        queuedDownload.setStatus(TransferStatus.DECRYPTING);
        decryptFile();
        queuedDownload.getMuxingCompletedLatch().countDown();
        queuedDownload.setStatus(TransferStatus.FINISHED);

        // ===================== STEP 3: REMOVE SHARDS AND ENCRYPTED FILE ===================== //

        // remove encrypted file
        if(queuedDownload.getEncryptedFile().exists()) {
            queuedDownload.getEncryptedFile().delete();
        }

        // remove shards
        for(Shard shard : queuedDownload.getShards()){
            File shardFile = new File(shard.getPath());

            if(shardFile.exists()) {
                shardFile.delete();
            }
        }
    }


    private void decryptFile(){

        String calculatedfileid = CryptoUtils.calculateFileId(queuedDownload.getBucketId(), queuedDownload.getUnencryptedFile().getName());

        try {
            cryptoUtils.decryptFile(queuedDownload.getEncryptedFile(), queuedDownload.getUnencryptedFile(), queuedDownload.getBucketId(), calculatedfileid);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("File download", "decryption completed: '" + queuedDownload.getUnencryptedFile().getName() + "' [Thread: " + Thread.currentThread().getName() + ", Thread id: " + Thread.currentThread().getId() + "]");
    }


    public Thread getThread(){
        return thisThread;
    }


}
