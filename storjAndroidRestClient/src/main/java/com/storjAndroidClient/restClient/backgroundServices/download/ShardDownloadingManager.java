package com.storjAndroidClient.restClient.backgroundServices.download;

import com.storjAndroidClient.restClient.backgroundServices.templateClasses.AllFilesTransferredListener;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.Shard;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.TransferStatus;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;
import com.storjAndroidClient.restClient.model.ShardPointer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Gabriel Comte on 17.5.2017.
 *
 * Singleton
 *
 * Manages the thread pool for shard downloads
 * Each ShardDownloadRunnable downloads one single shard
 */

public class ShardDownloadingManager {
    /*
     * Gets the number of available cores
     * (not always the same as the maximum number of cores)
     */
    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    // these variables do not have any effect when using blocking queues
    // Sets the amount of time an idle thread waits before terminating
    private static final int KEEP_ALIVE_TIME = 1;
    // Sets the Time Unit to seconds
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

    private static CryptoUtils cryptoUtils;

    // must be initialized as the last static variable
    private static final ShardDownloadingManager managerInstance = new ShardDownloadingManager();

    private QueuedDownload currentDownload;
    private AllFilesTransferredListener allFilesTransferredListener;
    private List<QueuedDownload> fileDownloadQueue;
    private List<QueuedDownload> currentlyDownloading;
    private boolean afterTransfer_executed;
    // Instantiates the queue of Runnables as a LinkedBlockingQueue
    // A queue of Runnables
    private final BlockingQueue<Runnable> queue;
    private final ThreadPoolExecutor shardDownloadThreadPool;

    public static ShardDownloadingManager getInstance(CryptoUtils cryptoUtils) {
        ShardDownloadingManager.cryptoUtils = cryptoUtils;
        return managerInstance;
    }

    private ShardDownloadingManager() {

        // Instantiates the queue of Runnables as a LinkedBlockingQueue
        // A queue of Runnables
        queue = new LinkedBlockingQueue<Runnable>();
        fileDownloadQueue = new ArrayList<QueuedDownload>();
        currentlyDownloading = new ArrayList<QueuedDownload>();
        afterTransfer_executed = false;

        // Creates a thread pool manager
        shardDownloadThreadPool = new ThreadPoolExecutor(
                NUMBER_OF_CORES,       // Initial pool size
                NUMBER_OF_CORES,       // Max pool size // does not have any effect when using blocking queues
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                queue);
    }

    public void addFileToQueue(final QueuedDownload queuedDownload, final AllFilesTransferredListener allFilesTransferredListener){

        System.out.println("xXx ADD FILE TO DOWNLOAD QUEUE xXx");

        this.allFilesTransferredListener = allFilesTransferredListener;

        new Thread() {
            @Override
            public void run() {
                System.out.println("Thread id in Downloading Manager addFileToQueue: " + Thread.currentThread().getId());

                System.out.println("is current download null? ==> ");
                System.out.println(currentDownload == null);

                if(currentDownload == null || currentDownload.getStatus().equals(TransferStatus.FAILED) || currentDownload.getStatus().equals(TransferStatus.CANCELED)){

                    // there is no download going on right now. Start the procedure.
                    currentDownload = queuedDownload;
                    currentDownload.setStatus(TransferStatus.WAITING_FOR_TRANSFER);
                    currentlyDownloading.add(currentDownload);

                    processQueue();

                }else {
                    fileDownloadQueue.add(queuedDownload);
                }
            }

        }.start();

    }

    public void processQueue(){

        System.out.println("xXx STARTED PROCESS QUEUE xXx");

        System.out.println(currentDownload);

        while(currentDownload != null) {

            ShardPointer shardPointer = (ShardPointer) currentDownload.getNextShardForTransfer();

            System.out.println(shardPointer);

            if (shardPointer != null && (currentDownload.getStatus() == TransferStatus.WAITING_FOR_TRANSFER || currentDownload.getStatus() == TransferStatus.TRANSFERRING)) {

                this.addShardToDownloadQueue(new ShardDownloadRunnable(currentDownload, shardPointer, cryptoUtils));

            } else {

                for(Shard shard : currentDownload.getShards()){
                    System.out.println("Shard " + shard.getIndex() +  ": " + shard.getTransferStatus());
                }

                // all shards for the current Download are being downloaded now

                if (this.fileDownloadQueue.size() > 0) {
                    // make next file from queue the current file
                    currentDownload = this.fileDownloadQueue.remove(0);
                    currentDownload.setStatus(TransferStatus.WAITING_FOR_TRANSFER);
                    currentlyDownloading.add(currentDownload);
                } else {

                    // remove files that have been transferred from currentlyTransferring list
                    List<QueuedDownload> new_currentlyDownloading = new ArrayList<QueuedDownload>();
                    for(QueuedDownload currentlyTransferring : currentlyDownloading){

                        if(currentlyTransferring.getStatus() == TransferStatus.WAITING || currentlyTransferring.getStatus() == TransferStatus.WAITING_FOR_TRANSFER || currentlyTransferring.getStatus() == TransferStatus.TRANSFERRING){
                            new_currentlyDownloading.add(currentlyTransferring);
                        }
                    }
                    currentlyDownloading = new_currentlyDownloading;

                    // add all downloads to a queue
                    currentlyDownloading.add(currentDownload);

                    // all shards have been added to the queue. Dont add anything anymore.
                    currentDownload = null;
                }
            }
        }
    }

    private void addShardToDownloadQueue(ShardDownloadRunnable shardDownloadRunnable) {
        shardDownloadThreadPool.execute(shardDownloadRunnable);
    }

    private void afterCompletingAllTransfers(){

        List<QueuedDownload> copy;
        do {
            copy = new ArrayList<>(currentlyDownloading);

            for (QueuedDownload currentlyTransferring : copy) {
                currentlyTransferring.waitForAllShardsToBeTransferred();
            }
        }
        while(!copy.equals(currentlyDownloading));

        // after all files have been downloaded, execute the on file transfered listener
        allFilesTransferredListener.onFileTransferOver();
    }

    /**
     * cancels all the items in the queue and running threads for a given download
     * @param downloadId
     */
    public void cancelAll(String downloadId) {
        /*
         * Creates an array of Runnables that's the same size as the
         * thread pool work queue
         */
        Runnable[] runnableArray = new Runnable[queue.size()];
        // Populates the array with the Runnables in the queue
        queue.toArray(runnableArray);

        // filter out the runnables belonging to the given download
        ShardDownloadRunnable[] runnableArrayGivenDownload = new ShardDownloadRunnable[queue.size()];
        int x = 0;
        for(Runnable r : runnableArray){
            ShardDownloadRunnable shardDownloadRunnable = (ShardDownloadRunnable) r;
            if(shardDownloadRunnable.belongsToDownload(downloadId)){
                // remove tasks from the queue
                queue.remove(shardDownloadRunnable);

                // add tasks to array
                runnableArrayGivenDownload[x++] = shardDownloadRunnable;
            }
        }

        /*
         * Iterates over the array of Runnables and interrupts each one's Thread.
         */
        synchronized (managerInstance) {
            // Iterates over the array of tasks
            for (int runnableIndex = 0; runnableIndex < runnableArrayGivenDownload.length; runnableIndex++) {
                // Gets the current thread
                Thread thread = (runnableArrayGivenDownload[runnableIndex]).getThread();
                // if the Thread exists, post an interrupt to it
                if (thread != null) {
                    thread.interrupt();
                }
            }
        }

        // remove file from currentlyTransferring list
        List<QueuedDownload> new_currentlyDownloading = new ArrayList<QueuedDownload>();
        for(QueuedDownload currentlyTransferring : currentlyDownloading){
            if(!currentlyTransferring.getTransferId().equals(downloadId)){
                new_currentlyDownloading.add(currentlyTransferring);
            }
        }
        currentlyDownloading = new_currentlyDownloading;

        if(currentlyDownloading.size() < 1 && queue.size() < 1){
            allFilesTransferredListener.onFileTransferOver();
        }

    }

    public void cancelAll() {
        /*
         * Creates an array of Runnables that's the same size as the
         * thread pool work queue
         */
        Runnable[] runnableArray = new Runnable[queue.size()];
        // Populates the array with the Runnables in the queue
        queue.toArray(runnableArray);



        // remove all tasks from the queue
        queue.removeAll(queue);
        /*
         * Iterates over the array of Runnables and interrupts each one's Thread.
         */
        synchronized (managerInstance) {
            // Iterates over the array of tasks
            for (int runnableIndex = 0; runnableIndex < runnableArray.length; runnableIndex++) {
                // Gets the current thread
                Thread thread = ((ShardDownloadRunnable) runnableArray[runnableIndex]).getThread();
                // if the Thread exists, post an interrupt to it
                if (thread != null) {
                    thread.interrupt();
                }
            }
        }

        allFilesTransferredListener.onFileTransferOver();
    }

}
