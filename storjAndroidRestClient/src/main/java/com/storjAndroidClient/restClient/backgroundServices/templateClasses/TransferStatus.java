package com.storjAndroidClient.restClient.backgroundServices.templateClasses;

/**
 * Created by Gabriel Comte on 23.11.2016.
 */

public enum TransferStatus{
    WAITING, SHARDING, ENCRYPTING, WAITING_FOR_TRANSFER, TRANSFERRING, CREATING_BUCKET_ENTRY, LOADING_TOKENS, DECRYPTING, DESHARDING, CANCELED, FAILED, FINISHED
}
