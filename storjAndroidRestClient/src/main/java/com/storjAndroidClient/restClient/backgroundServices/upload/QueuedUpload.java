package com.storjAndroidClient.restClient.backgroundServices.upload;

import android.util.Log;

import com.storjAndroidClient.restClient.backgroundServices.templateClasses.FileTransferListener;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.QueuedTransfer;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.Shard;
import com.storjAndroidClient.restClient.model.Frame;

import java.io.File;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Gabriel Comte on 06.11.2016.
 *
 * Queued upload needs a frame to be added to it. This feature is not provided in the constructor, because for performance reasons it makes more sense to set it later (create frame asynchronously)
 */

public class QueuedUpload extends QueuedTransfer {

    private Frame frame; // the frame of the upload
    private CountDownLatch frameCreatedLatch, nextShardCreatedLatch, shardingFinishedLatch, bucketEntryCreatedLatch;

    public QueuedUpload(File unencryptedFile, File encryptedFile, String bucketId, String fileId, FileTransferListener fileTransferListener){
        super( unencryptedFile, encryptedFile, bucketId, fileId, fileTransferListener);

        this.nextShardCreatedLatch = new CountDownLatch(1);
        this.frameCreatedLatch = new CountDownLatch(1);
        this.shardingFinishedLatch = new CountDownLatch(1);
        this.bucketEntryCreatedLatch = new CountDownLatch(1);
    }

    public Frame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        Log.d("File Upload", "frame created: '" + unencryptedFile.getName() + "' [Thread: " + Thread.currentThread().getName() + ", Thread id: " + Thread.currentThread().getId() + "]");
        this.frame = frame;
        frameCreatedLatch.countDown();
    }

    public CountDownLatch getFrameCreatedLatch() {
        return frameCreatedLatch;
    }

    public void setNextShardCreated() {
        Log.d("Sharding", "setNextShardCreated got called!" + " [Thread: " + Thread.currentThread().getName() + ", Thread id: " + Thread.currentThread().getId() + "]" );
        this.nextShardCreatedLatch.countDown();
    }

    public void initializeNextShardCreatedLatch() {
        this.nextShardCreatedLatch = new CountDownLatch(1);
    }

    public void waitForNextShardToBeCreated(int shardIndex){

        if(shardingFinishedLatch.getCount() == 0) {
            return;
        }

        // check if shard has already been created
        // this code is needed for the case that multiple shards had been created since processing the last shard.
        // ---> the next shard could already be ready, yet the CountDownLatch still on 1, because it is creating the next shard already
        for(Shard shard : this.shards) {
            if (shard.getIndex() == shardIndex) {
                this.initializeNextShardCreatedLatch();
                return;
            }
        }

        // if shard has not been created yet, wait for its creation
        try {
            this.nextShardCreatedLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.initializeNextShardCreatedLatch();
    }

    public void setTransferFailed() {
        super.setTransferFailed();

        // this latch needs to be released, if not the algorithm is stuck
        if(this.nextShardCreatedLatch.getCount() > 0) {
            this.nextShardCreatedLatch.countDown();
        }
    }

    public CountDownLatch getBucketEntryCreatedLatch() {
        return bucketEntryCreatedLatch;
    }

    public void setBucketEntryCreatedLatch(CountDownLatch bucketEntryCreatedLatch) {
        this.bucketEntryCreatedLatch = bucketEntryCreatedLatch;
    }

    public boolean isShardingFinished(){
        return (this.shardingFinishedLatch.getCount() < 1);
    }

    public void setShardingFinished(){
        this.shardingFinishedLatch.countDown();
    }

    @Override
    public String toString() {
        return "QueuedUpload{" +
                "frame=" + frame +
                ", nextShardCreatedLatch=" + nextShardCreatedLatch +
                ", frameCreatedLatch=" + frameCreatedLatch +
                ", shardingFinishedLatch=" + shardingFinishedLatch +
                ", bucketId='" + bucketId + "'" +
                super.toString() +
                '}';
    }
}