package com.storjAndroidClient.restClient.backgroundServices.upload;

import android.os.Process;
import android.util.Log;

import com.storjAndroidClient.restClient.Config;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.TransferStatus;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;
import com.storjAndroidClient.restClient.sharding.ShardingUtils;

import java.io.File;

/**
 * Created by Gabriel Comte on 15.3.2017.
 *
 * Encrypts a file and then it shards it
 * Designed to run on its own Thread.
 */

public class FileDemuxerRunnable implements Runnable {

    private Thread thisThread;
    private QueuedUpload queuedUpload;
    private CryptoUtils cryptoUtils;

    private File externalCacheDir;

//    public FileDemuxerRunnable(File inputFile, String bucketId, CryptoUtils cryptoUtils, File externalCacheDir, CountDownLatch firstShardCreatedLatch, CountDownLatch allShardsCreatedLatch) {
    public FileDemuxerRunnable(QueuedUpload queuedUpload, CryptoUtils cryptoUtils, File externalCacheDir) {

        this.queuedUpload = queuedUpload;
        this.cryptoUtils = cryptoUtils;
        this.externalCacheDir = externalCacheDir;

    }

    public boolean belongsToUpload(String uploadId){
        return uploadId.equals(queuedUpload.getTransferId());
    }

    @Override
    public void run() {

        // give the thread background priority but make it less prioritized than the uploading process.
        // The idea behind making it less favourable, is that as soon as a file is demuxed, the highest priority is to upload it, NOT to demuxed the next file
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND + Process.THREAD_PRIORITY_LESS_FAVORABLE);

        // ===================== STEP 1: ENCRYPTING FILE ===================== //
        if (Thread.interrupted()) {
            return;
        }
        if(queuedUpload.getStatus() == TransferStatus.WAITING) {
            queuedUpload.setStatus(TransferStatus.ENCRYPTING);
        }
        encryptFile();

        // ===================== STEP 2: SHARDING FILE ===================== //

        if (Thread.interrupted()) {
            return;
        }

        if(queuedUpload.getStatus() == TransferStatus.ENCRYPTING) {
            queuedUpload.setStatus(TransferStatus.SHARDING);
        }

        try {
            ShardingUtils.shardFile(queuedUpload, externalCacheDir, Config.MINIMAL_SHARD_SIZE, Config.MAXIMAL_SHARD_AMOUNT, Config.NUMBER_OF_CHALLENGES);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void encryptFile(){

        String calculatedfileid = CryptoUtils.calculateFileId(queuedUpload.getBucketId(), queuedUpload.getUnencryptedFile().getName());

        try {
            cryptoUtils.encryptFile(queuedUpload.getUnencryptedFile(), queuedUpload.getEncryptedFile(), queuedUpload.getBucketId(), calculatedfileid);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("File upload", "encryption completed: '" + queuedUpload.getEncryptedFile().getName() + "' [Thread: " + Thread.currentThread().getName() + ", Thread id: " + Thread.currentThread().getId() + "]");
    }


    public Thread getThread(){
        return thisThread;
    }


}
