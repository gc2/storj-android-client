package com.storjAndroidClient.restClient.backgroundServices.templateClasses;

/**
 * Created by Gabriel Comte on 26.05.2017.
 */

public enum ShardStatus {
    WAITING, TRANSFERRING, TRANSFERRED, CANCELED, FAILED
}
