package com.storjAndroidClient.restClient.backgroundServices.templateClasses;

import com.storjAndroidClient.restClient.auth.AuthenticationFailedListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

/**
 * Model class for QueuedUpload and QueuedDownload
 *
 * Created by Gabriel Comte on 23.11.2016.
 */

abstract public class QueuedTransfer {
    protected String transferId; // unique id for each specific transfer (upload / download)
    protected CountDownLatch allShardsTransferLatch; // will be counted down by service as soon as all shards are transferred or the transfer failed
    protected TransferStatus status; // Upload: for checking whether there are any shards left to be generated
    protected File unencryptedFile, encryptedFile;
    protected List<Shard> shards;

    // used for encryption / decryption
    protected String bucketId;
    protected String fileId;

    // listeners
    protected FileTransferListener fileTransferListener;
    protected AuthenticationFailedListener authenticationFailedListener;

    public QueuedTransfer(File unencryptedFile, File encryptedFile, String bucketId, String fileId, FileTransferListener fileTransferListener){
        this.status = TransferStatus.WAITING;

        this.shards = new ArrayList<Shard>();
        this.transferId = UUID.randomUUID().toString();
        this.allShardsTransferLatch = new CountDownLatch(1);
        this.unencryptedFile = unencryptedFile;
        this.encryptedFile = encryptedFile;
        this.bucketId = bucketId;
        this.fileId = fileId;
        this.fileTransferListener = fileTransferListener;
    }

    public void waitForAllShardsToBeTransferred() {
        try {
            allShardsTransferLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setTransferFailed() {
        this.status = TransferStatus.FAILED;
        this.allShardsTransferLatch.countDown();
    }

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String uploadId) {
        this.transferId = uploadId;
    }

    public TransferStatus getStatus() {
        return status;
    }

    public void setStatus(TransferStatus status) {
        this.status = status;
    }

    public File getEncryptedFile() {
        return encryptedFile;
    }

    public void setEncryptedFile(File encryptedFile) {
        this.encryptedFile = encryptedFile;
    }

    public String getBucketId() {
        return bucketId;
    }

    public void setBucketId(String bucketId) {
        this.bucketId = bucketId;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public FileTransferListener getFileTransferListener() {
        return fileTransferListener;
    }

    public void setFileTransferListener(FileTransferListener fileTransferListener) {
        this.fileTransferListener = fileTransferListener;
    }

    public AuthenticationFailedListener getAuthenticationFailedListener() {
        return authenticationFailedListener;
    }

    public void setAuthenticationFailedListener(AuthenticationFailedListener failedSignatureAuthenticationListener) {
        this.authenticationFailedListener = failedSignatureAuthenticationListener;
    }

    public File getUnencryptedFile() {
        return unencryptedFile;
    }

    public void setUnencryptedFile(File unencryptedFile) {
        this.unencryptedFile = unencryptedFile;
    }

    public synchronized List<Shard> getShards() {
        return shards;
    }

    public synchronized void setShards(List<Shard> shards) {
        this.shards = shards;
    }

    public synchronized void addShard(Shard shard) {
        this.shards.add(shard);
    }

    public CountDownLatch getAllShardsTransferLatch() {
        return allShardsTransferLatch;
    }

    public void setAllShardsTransferLatch(CountDownLatch allShardsTransferLatch) {
        this.allShardsTransferLatch = allShardsTransferLatch;
    }

    // pops the first added shard and removes it from the list
    public synchronized Shard getNextShardForTransfer() {

        System.out.println("shards.size(): " + shards.size());

        for(int x = 0; x < shards.size(); x++){
            if(shards.get(x).getTransferStatus() == ShardStatus.WAITING){
                shards.get(x).setTransferStatus(ShardStatus.TRANSFERRING);
                return shards.get(x);
            }
        }

        // there are no more shards to be transferred for this file upload / download
        return null;
    }

    @Override
    public String toString() {
        return  "transferId='" + transferId + '\'' +
                ", allShardsTransferLatch=" + allShardsTransferLatch +
                ", status=" + status +
                ", encryptedFile=" + encryptedFile +
                ", bucketId='" + bucketId + '\'' +
                ", fileId='" + fileId + '\'' +
                ", fileTransferListener=" + fileTransferListener +
                ", authenticationFailedListener=" + authenticationFailedListener;
    }
}

