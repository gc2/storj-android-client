package com.storjAndroidClient.restClient.backgroundServices.upload;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.storjAndroidClient.restClient.Config;
import com.storjAndroidClient.restClient.StorjRestClient;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.AllFilesTransferredListener;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.Shard;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.TransferStatus;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;
import com.storjAndroidClient.restClient.model.ShardUploadParameters;
import com.storjAndroidClient.restClient.model.UploadableShard;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Gabriel Comte on 25.3.2017.
 *
 * Singleton
 *
 * Manages the thread pool for shard uploads
 * Each ShardUploadRunnable uploads one single shard
 */

public class ShardUploadingManager {
    /*
     * Gets the number of available cores
     * (not always the same as the maximum number of cores)
     */
    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    // these variables do not have any effect when using blocking queues
    // Sets the amount of time an idle thread waits before terminating
    private static final int KEEP_ALIVE_TIME = 1;
    // Sets the Time Unit to seconds
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

    private static CryptoUtils cryptoUtils;

    // must be initialized as the last static variable
    private static final ShardUploadingManager managerInstance = new ShardUploadingManager();

    private QueuedUpload currentUpload;
    private AllFilesTransferredListener allFilesTransferredListener;
    private List<QueuedUpload> fileUploadQueue;
    private List<QueuedUpload> currentlyUploading;
    private boolean afterTransfer_executed;
    // Instantiates the queue of Runnables as a LinkedBlockingQueue
    // A queue of Runnables
    private final BlockingQueue<Runnable> queue;
    private final ThreadPoolExecutor shardUploadThreadPool;

    public static ShardUploadingManager getInstance(CryptoUtils cryptoUtils) {
        ShardUploadingManager.cryptoUtils = cryptoUtils;
        return managerInstance;
    }

    private ShardUploadingManager() {

        // Instantiates the queue of Runnables as a LinkedBlockingQueue
        // A queue of Runnables
        queue = new LinkedBlockingQueue<Runnable>();
        fileUploadQueue = new ArrayList<QueuedUpload>();
        currentlyUploading = new ArrayList<QueuedUpload>();
        afterTransfer_executed = false;

        // Creates a thread pool manager
        shardUploadThreadPool = new ThreadPoolExecutor(
                NUMBER_OF_CORES,       // Initial pool size
                NUMBER_OF_CORES,       // Max pool size // does not have any effect when using blocking queues
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                queue);
    }

    public void addFileToQueue(final QueuedUpload queuedUpload, final AllFilesTransferredListener allFilesTransferredListener){

        System.out.println("xXx ADD FILE TO QUEUE xXx");

        this.allFilesTransferredListener = allFilesTransferredListener;

        new Thread() {
            @Override
            public void run() {
                System.out.println("Thread id in Uploading Manager addFileToQueue: " + Thread.currentThread().getId());

                // wait for frame to be created
                try {
                    System.out.println("1 getFrameCreatedLatch().getCount: " + queuedUpload.getFrameCreatedLatch().getCount());
                    queuedUpload.getFrameCreatedLatch().await();
                    System.out.println("2 getFrameCreatedLatch().getCount: " + queuedUpload.getFrameCreatedLatch().getCount());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("is current upload null? ==> ");
                System.out.println(currentUpload == null);

                if(currentUpload == null || currentUpload.getStatus().equals(TransferStatus.FAILED) || currentUpload.getStatus().equals(TransferStatus.CANCELED)){

                    // there is no upload going on right now. Start the procedure.
                    currentUpload = queuedUpload;
                    currentUpload.setStatus(TransferStatus.WAITING_FOR_TRANSFER);
                    currentlyUploading.add(currentUpload);

                    processQueue();

                    // start the method which listens to the all files uploaded event.
                    if(!afterTransfer_executed){
                        System.out.println("initializing afterCompletingAllTransfers()");
                        afterTransfer_executed = true;
                        afterCompletingAllTransfers();
                        afterTransfer_executed = false;
                    }
                }else {
                    fileUploadQueue.add(queuedUpload);
                }
            }

        }.start();

    }

    public void processQueue(){

        System.out.println("xXx STARTED PROCESS QUEUE xXx");

        int shardIndex = 0;

        while(currentUpload != null) {

            System.out.println("WAITING FOR SHARD NUMBER: " + shardIndex);

            // wait until next shard has been created
            currentUpload.waitForNextShardToBeCreated(shardIndex++);

            System.out.println("currentUpload.getStatus(): " + currentUpload.getStatus());

            final UploadableShard uploadableShard = (UploadableShard) currentUpload.getNextShardForTransfer();
            if (uploadableShard != null && (currentUpload.getStatus() == TransferStatus.WAITING_FOR_TRANSFER || currentUpload.getStatus() == TransferStatus.TRANSFERRING)) {

                // add the shard to the corresponding frame on the Bridge
                try {

                    StorjRestClient.getInstance().addShardToFrame(currentUpload, uploadableShard, Config.ADD_SHARD_TO_FRAME_RETRIES, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Gson gson = new Gson();
                            uploadableShard.setUploadParameters(gson.fromJson(response.toString(), ShardUploadParameters.class));
                            uploadableShard.getAddedToFrameLatch().countDown();
                        }


                    }, this.currentUpload.getFileTransferListener());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    uploadableShard.getAddedToFrameLatch().await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }



                // kommt bei error nicht hier hin

                System.out.println("kommt hier...???? nach dem dingens?");
                System.out.println("currentUpload.getStatus(): " + currentUpload.getStatus());
                System.out.println("currentUpload: " + currentUpload);


                if(currentUpload.getStatus() == TransferStatus.WAITING_FOR_TRANSFER || currentUpload.getStatus() == TransferStatus.TRANSFERRING) {
                    this.addShardToUploadQueue(new ShardUploadRunnable(currentUpload, uploadableShard, cryptoUtils));
                }
            } else {

                for(Shard shard : currentUpload.getShards()){
                    System.out.println("Shard " + shard.getIndex() +  ": " + shard.getTransferStatus());
                }

                // there are no more shards for the current Upload to be added to the queue.

                if (this.fileUploadQueue.size() > 0) {
                    // make next file from queue the current file
                    currentUpload = this.fileUploadQueue.remove(0);
                    currentUpload.setStatus(TransferStatus.WAITING_FOR_TRANSFER);
                    currentlyUploading.add(currentUpload);
                } else {

                    // remove files that have been transferred from currentlyTransferring list
                    List<QueuedUpload> new_currentlyUploading = new ArrayList<QueuedUpload>();
                    for(QueuedUpload currentlyTransferring : currentlyUploading){

                        if(currentlyTransferring.getStatus() == TransferStatus.WAITING || currentlyTransferring.getStatus() == TransferStatus.WAITING_FOR_TRANSFER || currentlyTransferring.getStatus() == TransferStatus.TRANSFERRING){
                            new_currentlyUploading.add(currentlyTransferring);
                        }
                    }
                    currentlyUploading = new_currentlyUploading;

                    // add all uploads to a queue
                    currentlyUploading.add(currentUpload);

                    // all shards have been added to the queue. Dont add anything anymore.
                    currentUpload = null;
                }
            }
        }
    }

    private void addShardToUploadQueue(ShardUploadRunnable shardUploadRunnable) {
        shardUploadThreadPool.execute(shardUploadRunnable);
    }

    private void afterCompletingAllTransfers(){

        List<QueuedUpload> copy;
        do {
            copy = new ArrayList<>(currentlyUploading);

            for (QueuedUpload currentlyTransferring : copy) {
                currentlyTransferring.waitForAllShardsToBeTransferred();
            }
        }
        while(!copy.equals(currentlyUploading));

        // after all files have been uploaded, execute the on file transfered listener
        allFilesTransferredListener.onFileTransferOver();
    }

    /**
     * cancels all the items in the queue and running threads for a given upload
     * @param uploadId
     */
    public void cancelAll(String uploadId) {
        /*
         * Creates an array of Runnables that's the same size as the
         * thread pool work queue
         */
        Runnable[] runnableArray = new Runnable[queue.size()];
        // Populates the array with the Runnables in the queue
        queue.toArray(runnableArray);

        // filter out the runnables belonging to the given upload
        ShardUploadRunnable[] runnableArrayGivenUpload = new ShardUploadRunnable[queue.size()];
        int x = 0;
        for(Runnable r : runnableArray){
            ShardUploadRunnable shardUploadRunnable = (ShardUploadRunnable) r;
            if(shardUploadRunnable.belongsToUpload(uploadId)){
                // remove tasks from the queue
                queue.remove(shardUploadRunnable);

                // add tasks to array
                runnableArrayGivenUpload[x++] = shardUploadRunnable;
            }
        }

        /*
         * Iterates over the array of Runnables and interrupts each one's Thread.
         */
        synchronized (managerInstance) {
            // Iterates over the array of tasks
            for (int runnableIndex = 0; runnableIndex < runnableArrayGivenUpload.length; runnableIndex++) {
                // Gets the current thread
                Thread thread = (runnableArrayGivenUpload[runnableIndex]).getThread();
                // if the Thread exists, post an interrupt to it
                if (thread != null) {
                    thread.interrupt();
                }
            }
        }

        // remove file from currentlyTransferring list
        List<QueuedUpload> new_currentlyUploading = new ArrayList<QueuedUpload>();
        for(QueuedUpload currentlyTransferring : currentlyUploading){
            if(!currentlyTransferring.getTransferId().equals(uploadId)){
                new_currentlyUploading.add(currentlyTransferring);
            }
        }
        currentlyUploading = new_currentlyUploading;

        if(currentlyUploading.size() < 1 && queue.size() < 1){
            allFilesTransferredListener.onFileTransferOver();
        }

    }

    public void cancelAll() {
        /*
         * Creates an array of Runnables that's the same size as the
         * thread pool work queue
         */
        Runnable[] runnableArray = new Runnable[queue.size()];
        // Populates the array with the Runnables in the queue
        queue.toArray(runnableArray);



        // remove all tasks from the queue
        queue.removeAll(queue);
        /*
         * Iterates over the array of Runnables and interrupts each one's Thread.
         */
        synchronized (managerInstance) {
            // Iterates over the array of tasks
            for (int runnableIndex = 0; runnableIndex < runnableArray.length; runnableIndex++) {
                // Gets the current thread
                Thread thread = ((ShardUploadRunnable) runnableArray[runnableIndex]).getThread();
                // if the Thread exists, post an interrupt to it
                if (thread != null) {
                    thread.interrupt();
                }
            }
        }

        allFilesTransferredListener.onFileTransferOver();
    }

}
