package com.storjAndroidClient.restClient.backgroundServices.download;

import com.storjAndroidClient.restClient.backgroundServices.upload.FileDemuxerRunnable;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Gabriel Comte on 15.3.2017.
 *
 * Singleton
 *
 * Manages the thread pool for file demuxers (encrypting and sharding files)
 * Each FileDemuxerRunnable encrypts and shards one file
 */

public class FileMuxingManager {
    /*
     * Gets the number of available cores
     * (not always the same as the maximum number of cores)
     */
    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    // these variables do not have any effect when using blocking queues
    // Sets the amount of time an idle thread waits before terminating
    private static final int KEEP_ALIVE_TIME = 1;
    // Sets the Time Unit to seconds
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

    // must be initialized as the last static variable
    private static final FileMuxingManager managerInstance = new FileMuxingManager();

    // Instantiates the queue of Runnables as a LinkedBlockingQueue
    // A queue of Runnables
    private final BlockingQueue<Runnable> queue;
    private final ThreadPoolExecutor demuxerThreadPool;

    public static FileMuxingManager getInstance() {
        return managerInstance;
    }

    private FileMuxingManager() {

        // Instantiates the queue of Runnables as a LinkedBlockingQueue
        // A queue of Runnables
        queue = new LinkedBlockingQueue<Runnable>();

        // Creates a thread pool manager
        demuxerThreadPool = new ThreadPoolExecutor(
                NUMBER_OF_CORES,       // Initial pool size
                NUMBER_OF_CORES,       // Max pool size // does not have any effect when using blocking queues
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                queue);
    }

    public void addToMuxQueue(FileMuxerRunnable fileMuxerRunnable) {
        demuxerThreadPool.execute(fileMuxerRunnable);

//        queue.size()

//        // start the method which listens to the all files downloaded event.
//        if(!afterTransfer_executed){
//            System.out.println("initializing afterCompletingAllTransfers()");
//            afterTransfer_executed = true;
//            afterCompletingAllTransfers();
//            afterTransfer_executed = false;
//        }
    }

    /**
     * * cancels all the items in the queue and running threads for a given upload
     * @param uploadId
     */
    public void cancelAll(String uploadId) {
        /*
         * Creates an array of Runnables that's the same size as the
         * thread pool work queue
         */
        Runnable[] runnableArray = new Runnable[queue.size()];
        // Populates the array with the Runnables in the queue
        queue.toArray(runnableArray);

        // filter out the runnables belonging to the given download
        FileMuxerRunnable[] runnableArrayGivenDownload = new FileMuxerRunnable[queue.size()];
        int x = 0;
        for(Runnable r : runnableArray){
            FileMuxerRunnable fileMuxerRunnable = (FileMuxerRunnable) r;
            if(fileMuxerRunnable.belongsToDownload(uploadId)){
                // remove tasks from the queue
                queue.remove(fileMuxerRunnable);

                // add tasks to array
                runnableArrayGivenDownload[x++] = fileMuxerRunnable;
            }
        }

        /*
         * Iterates over the array of Runnables and interrupts each one's Thread.
         */
        synchronized (managerInstance) {
            // Iterates over the array of tasks
            for (int runnableIndex = 0; runnableIndex < runnableArrayGivenDownload.length; runnableIndex++) {
                // Gets the current thread
                Thread thread = (runnableArrayGivenDownload[runnableIndex]).getThread();
                // if the Thread exists, post an interrupt to it
                if (thread != null) {
                    thread.interrupt();
                }
            }
        }
    }

    public void cancelAll() {
        /*
         * Creates an array of Runnables that's the same size as the
         * thread pool work queue
         */
        Runnable[] runnableArray = new Runnable[queue.size()];
        // Populates the array with the Runnables in the queue
        queue.toArray(runnableArray);

        // remove all tasks from the queue
        queue.removeAll(queue);
        /*
         * Iterates over the array of Runnables and interrupts each one's Thread.
         */
        synchronized (managerInstance) {
            // Iterates over the array of tasks
            for (int runnableIndex = 0; runnableIndex < runnableArray.length; runnableIndex++) {
                // Gets the current thread
                Thread thread = ((FileMuxerRunnable) runnableArray[runnableIndex]).getThread();
                // if the Thread exists, post an interrupt to it
                if (thread != null) {
                    thread.interrupt();
                }
            }
        }
    }

}
