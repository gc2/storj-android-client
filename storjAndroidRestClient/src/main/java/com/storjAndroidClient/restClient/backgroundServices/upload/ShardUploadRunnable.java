package com.storjAndroidClient.restClient.backgroundServices.upload;

import android.os.Process;

import com.storjAndroidClient.restClient.backgroundServices.templateClasses.ExchangeReportSender;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.Shard;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.ShardStatus;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.TransferStatus;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;
import com.storjAndroidClient.restClient.fileHandling.fileTransfer.HttpShardTransfer;
import com.storjAndroidClient.restClient.model.ExchangeReportMessage;
import com.storjAndroidClient.restClient.model.ExchangeReportResult;
import com.storjAndroidClient.restClient.model.UploadableShard;

import java.io.File;

/**
 * Created by Gabriel Comte on 15.3.2017.
 *
 * Uploads a shard
 * Designed to run on an its own Thread.
 */

public class ShardUploadRunnable implements Runnable {

    private Thread thisThread;

    private QueuedUpload queuedUpload;
    private UploadableShard uploadableShard;
    private static CryptoUtils cryptoUtils;

    public ShardUploadRunnable(QueuedUpload queuedUpload, UploadableShard uploadableShard, CryptoUtils cryptoUtils) {

        this.queuedUpload = queuedUpload;
        this.uploadableShard = uploadableShard;
        ShardUploadRunnable.cryptoUtils = cryptoUtils;
    }

    public boolean belongsToUpload(String uploadId){
        return uploadId.equals(queuedUpload.getTransferId());
    }

    @Override
    public void run() {

        thisThread = Thread.currentThread();

        // give the thread background priority. With this priority, it is still more prioritized than the sharding process.
        // The idea behind making it more favourable, is that as soon as a file is demuxed, the highest priority is to upload it, NOT to demuxed any next file
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

        if (Thread.interrupted()) {
            return;
        }

        // ===================== STEP 1: UPLOAD SHARD TO FARMER ===================== //

        if(queuedUpload.getStatus() == TransferStatus.WAITING_FOR_TRANSFER){
            queuedUpload.setStatus(TransferStatus.TRANSFERRING);
        }

        int shardUploadStart = Math.round(System.currentTimeMillis() / 1000L); // unix timestamp
        int shardUploadEnd;
        boolean uploadSuccessful = false;

        try {
            HttpShardTransfer.sendShard(uploadableShard);
            uploadSuccessful = true;

            System.out.println("queuedUpload.isShardingFinished(): " + queuedUpload.isShardingFinished());

            // if all shards have been created [sharding is finished] and all shards have the status TRANSFERRED, then the file is uploaded and the file entry can be created on the bridge
            if(queuedUpload.isShardingFinished()) {
                boolean allShardsTransferred = true;
                for (Shard shard : queuedUpload.getShards()) {
                    if (shard.getTransferStatus() != ShardStatus.TRANSFERRED) {
                        allShardsTransferred = false;
                    }
                }
                if (allShardsTransferred) {
                    queuedUpload.getAllShardsTransferLatch().countDown();
                    queuedUpload.setStatus(TransferStatus.CREATING_BUCKET_ENTRY);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {

            // ===================== STEP 2: SEND EXCHANGE REPORT ===================== //

            shardUploadEnd = Math.round(System.currentTimeMillis() / 1000L); // unix timestamp

            if(uploadSuccessful) {
                System.out.println("Shard-upload successful");
                // send exchange report [upload was successful]
                ExchangeReportSender.sendExchangeReport(shardUploadStart, shardUploadEnd, uploadableShard, cryptoUtils, uploadableShard.getUploadParameters().getFarmer().getNodeID(), ExchangeReportResult.SUCCESS, ExchangeReportMessage.SHARD_UPLOADED);
            }else{
                System.out.println("Shard-upload unsuccessful");
                queuedUpload.setStatus(TransferStatus.FAILED);

                // send exchange report [upload was unsuccessful] - it is not clear yet whether this exchange must be sent or only the one on success (Stand 17.02.2017)
                ExchangeReportSender.sendExchangeReport(shardUploadStart, shardUploadEnd, uploadableShard, cryptoUtils, uploadableShard.getUploadParameters().getFarmer().getNodeID(), ExchangeReportResult.FAILURE, ExchangeReportMessage.SHARD_UPLOADED);

                // upload failed, cleanup everything
                queuedUpload.getFileTransferListener().onTransferFailed(queuedUpload);
            }


            // ===================== STEP 3: CLEAN UP SHARD FILE ===================== //

            File shardFile = new File(uploadableShard.getPath());
            shardFile.delete();
        }
    }

    public Thread getThread(){
        return thisThread;
    }


}
