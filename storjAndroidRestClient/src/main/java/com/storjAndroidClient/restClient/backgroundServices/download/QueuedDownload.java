package com.storjAndroidClient.restClient.backgroundServices.download;

import com.storjAndroidClient.restClient.backgroundServices.templateClasses.FileTransferListener;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.QueuedTransfer;

import java.io.File;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Gabriel Comte on 22.11.2016.
 */

public class QueuedDownload extends QueuedTransfer {

    private String mimeType;
    private CountDownLatch muxingCompletedLatch;

    public QueuedDownload(File unencryptedFile, File encryptedFile, String bucketId, String fileId, FileTransferListener fileTransferListener) {
        super(unencryptedFile, encryptedFile, bucketId, fileId, fileTransferListener);
        this.muxingCompletedLatch = new CountDownLatch(1);
    }

    public CountDownLatch getMuxingCompletedLatch() {
        return muxingCompletedLatch;
    }

    public void setMuxingCompletedLatch(CountDownLatch muxingCompletedLatch) {
        this.muxingCompletedLatch = muxingCompletedLatch;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public String toString() {
        return "QueuedDownload{" +
                super.toString() +
                ", muxingCompletedLatch=" + muxingCompletedLatch +
                "}";
    }
}
