package com.storjAndroidClient.restClient.backgroundServices.templateClasses;

/**
 * Model class for UploadableShard and ShardPointer
 *
 * Created by Gabriel Comte on 27.11.2016.
 */

abstract public class Shard {

    // should be excluded by GSON => transient (http://stackoverflow.com/questions/4802887/gson-how-to-exclude-specific-fields-from-serialization-without-annotations)
    private transient String transferId; // unique id for each specific file transfer (upload / download)
    private transient ShardStatus transferStatus; // unique id for each specific file transfer (upload / download)
    private transient String path; // where the shard is stored

    private String hash;
    private long size;
    private int index;
    private boolean parity;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public ShardStatus getTransferStatus() {
        return transferStatus;
    }

    public void setTransferStatus(ShardStatus transferStatus) {
        this.transferStatus = transferStatus;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isParity() {
        return parity;
    }

    public void setParity(boolean parity) {
        this.parity = parity;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((transferId == null) ? 0 : transferId.hashCode());
        result = prime * result + ((hash == null) ? 0 : hash.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (!(obj instanceof Shard))
            return false;
        if(obj == null)
            return false;


        Shard shard = (Shard) obj;

        if(!shard.getTransferId().equals(this.getTransferId())){
            return false;
        }
        if(!shard.getHash().equals(this.getHash())){
            return false;
        }
        if(shard.getSize() != this.getSize()){
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return  "index=" + index +
                ", size=" + size +
                ", transferId='" + transferId + '\'' +
                ", hash='" + hash + '\'' +
                ", parity=" + parity;
    }
}

