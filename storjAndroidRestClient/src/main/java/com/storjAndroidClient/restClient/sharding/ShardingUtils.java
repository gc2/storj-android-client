package com.storjAndroidClient.restClient.sharding;

import android.util.Log;

import com.google.common.base.Charsets;
import com.google.common.primitives.Bytes;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.Shard;
import com.storjAndroidClient.restClient.backgroundServices.templateClasses.ShardStatus;
import com.storjAndroidClient.restClient.backgroundServices.upload.QueuedUpload;
import com.storjAndroidClient.restClient.fileHandling.encryption.CryptoUtils;
import com.storjAndroidClient.restClient.helperClasses.FileSizeUtils;
import com.storjAndroidClient.restClient.helperClasses.JavaNioFiles;
import com.storjAndroidClient.restClient.model.UploadableShard;

import org.spongycastle.util.Arrays;
import org.spongycastle.util.encoders.Hex;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Gabriel Comte on 22.10.2016.
 */

public class ShardingUtils {

    /**
     * results the shard size in a way that shards are not smaller than [minimalShardSize]
     * and sharding results in a maximum of [maximalShardAmount] shards.
     *
     * @param fileSize in MegaBytes
     * @param minimalShardSize in MegaBytes
     * @param maximalShardAmount in MegaBytes
     * @return
     */
    public static int calculateShardSize(long fileSize, int minimalShardSize, int maximalShardAmount){

        double unroundedShardSize = fileSize / maximalShardAmount;

        int result = 1;
        while(unroundedShardSize > Math.pow(2, result)){
            result++;
        }

        if(result < minimalShardSize){
            return minimalShardSize;
        }else{
            return (int) Math.pow(2, result);
        }

    }

    /**
     * Utility method to split a file into several shards.
     * @param minimalShardSize the minimal size of a file(in MegaBytes) to even shard
     * @param maximalShardAmount the maximum amount of shards that should be created by sharding
     * @param targetFolder where the shards are stored in
     * @throws Exception problem sharding the file.
     */
    public static void shardFile(QueuedUpload queuedUpload, File targetFolder, int minimalShardSize, int maximalShardAmount, int amountOfChallenges) throws Exception {

        int shardSizeInBytes = 1024 * 1024 * calculateShardSize(queuedUpload.getEncryptedFile().length() / 1024 / 1024, minimalShardSize, maximalShardAmount);
        Log.d("File upload", "Start sharding file '" + queuedUpload.getEncryptedFile().getName() + "' [File size: " + FileSizeUtils.humanReadableByteCount(queuedUpload.getEncryptedFile().length(), false) + ", Shard size: " + FileSizeUtils.humanReadableByteCount(shardSizeInBytes, false) + ", Amount of Shards: " + (((int) (queuedUpload.getEncryptedFile().length() / shardSizeInBytes)) + 1) + ", Thread: " + Thread.currentThread().getName() + ", Thread id: " + Thread.currentThread().getId() + "]");

        if(queuedUpload.getEncryptedFile().length() < shardSizeInBytes){
            // no sharding needed. Just add the encrypted file as a shard

            // create uploadableShard object pointing to the file.
            UploadableShard uploadableShard = new UploadableShard();
            uploadableShard.setTransferId(queuedUpload.getTransferId());
            uploadableShard.setSize(queuedUpload.getEncryptedFile().length());
            uploadableShard.setHash(new String(CryptoUtils.getRipemdSha256File(queuedUpload.getEncryptedFile())));
            uploadableShard.setPath(queuedUpload.getEncryptedFile().getAbsolutePath());
            uploadableShard.setIndex(0);
            addChallenges(uploadableShard, new byte[shardSizeInBytes], amountOfChallenges);
            uploadableShard.setTransferId(queuedUpload.getTransferId());
            uploadableShard.setTransferStatus(ShardStatus.WAITING);
            queuedUpload.addShard(uploadableShard);
            queuedUpload.setNextShardCreated();

        }else{
            // start sharding

            FileInputStream inputStream = new FileInputStream(queuedUpload.getEncryptedFile());
            byte[] buffer = new byte[shardSizeInBytes];
            int length;
            int shardIndex = 0;

            // read file, one "buffer" at a time. The buffer is set to the shard size
            // So each read is 1 shard :).
            while ((length = inputStream.read(buffer)) > 0){

                // Write the file
                File fileShard = new File(targetFolder.getPath() + File.separator + queuedUpload.getTransferId() + "_" + shardIndex + ".shard");
                FileOutputStream out = new FileOutputStream(fileShard);
                out.write(buffer, 0, length);
                out.close();

                // create uploadableShard object pointing to the file.
                UploadableShard uploadableShard = new UploadableShard();
                uploadableShard.setTransferId(queuedUpload.getTransferId());
                uploadableShard.setSize(fileShard.length());
                uploadableShard.setHash(new String(CryptoUtils.getRipemdSha256File(fileShard)));
                uploadableShard.setPath(fileShard.getAbsolutePath());
                uploadableShard.setIndex(shardIndex++);
                addChallenges(uploadableShard, buffer, amountOfChallenges);
                uploadableShard.setTransferId(queuedUpload.getTransferId());
                uploadableShard.setTransferStatus(ShardStatus.WAITING);

                queuedUpload.addShard(uploadableShard);

                queuedUpload.setNextShardCreated();
            }
        }

        queuedUpload.setShardingFinished();
    }

    /**
     * Create challenges for a uploadableShard.
     * @param uploadableShard the uploadableShard to create challenges for.
     * @param shardData the data contained in the uploadableShard.
     * @param numberOfChallenges the number of challenges.
     */
    public static void addChallenges(UploadableShard uploadableShard, byte[] shardData, int numberOfChallenges){
        for(int i = 0; i< numberOfChallenges; i++) {

            Log.d("Create Challenges", "Shard [" + uploadableShard.getTransferId() + "_" + uploadableShard.getIndex() + "]: create challenge nr. " + (i + 1) + " [Thread: " + Thread.currentThread().getName() + ", Thread id: " + Thread.currentThread().getId() + "]");

            String randomChallenge = getRandomChallengeString();
            byte[] challengeBytes = randomChallenge.getBytes(Charsets.UTF_8);

            // trim empty space at the end of shardData.
            shardData = Arrays.copyOf(shardData, (int) uploadableShard.getSize());

            // Data to hash = challenge + uploadableShard data.
            byte[] dataToHash = Hex.encode(Bytes.concat(challengeBytes, shardData));

            // RMD160(SHA256(RMD160(SHA256(challenge + uploadableShard))))
            byte[] tree = CryptoUtils.rmd160Sha256_noHexEncode(Hex.encode(CryptoUtils.rmd160Sha256_noHexEncode(dataToHash)));

            uploadableShard.getChallenges().add(randomChallenge);
            uploadableShard.getTree().add(CryptoUtils.bytesToHex(tree));
        }
    }

    /**
     * Creates a random 32 byte string.
     * @return a random 32 byte string.
     */
    public static String getRandomChallengeString(){
        int numChars = 32;

        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        while(sb.length() < numChars){
            sb.append(Integer.toHexString(r.nextInt()));
        }
        return sb.toString().substring(0, numChars);
    }

    /**
     * Take several shards and put them back together into one file.
     * Note: this does not decrypt the file.
     * @param shards the shards.
     * @param destination the destination to put the combined file.
     * @throws IOException Problem reading/writing.
     */
    public static void pieceTogetherShards(List<Shard> shards, File destination) throws IOException {

        List<File> shardList = new ArrayList<File>();

        for(Shard shard : shards){
            shardList.add(new File(shard.getPath()));
        }

        pieceTogetherFiles(shardList, destination);
    }

    /**
     * Take several shards and put them back together into one file.
     * Note: this does not decrypt the file.
     * @param shards the shards as files
     * @param destination the destination to put the combined file.
     * @throws IOException Problem reading/writing.
     */
    public static void pieceTogetherFiles(List<File> shards, File destination) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(destination);
        for(File shard : shards) {
            byte[] shardBytes = JavaNioFiles.readAllBytes(shard.getPath());
            outputStream.write(shardBytes);
        }
        outputStream.close();
    }
}
