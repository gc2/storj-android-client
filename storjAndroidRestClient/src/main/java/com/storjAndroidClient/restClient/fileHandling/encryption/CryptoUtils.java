package com.storjAndroidClient.restClient.fileHandling.encryption;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.google.common.hash.Hashing;
import com.storjAndroidClient.restClient.Config;
import com.storjAndroidClient.restClient.model.EncryptionValues;

import org.apache.commons.lang3.ArrayUtils;
import org.spongycastle.crypto.digests.RIPEMD160Digest;
import org.spongycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.spongycastle.util.encoders.Hex;

import java.io.File;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;

/**
 * Created by Gabriel Comte on 14.10.2016.
 */

public class CryptoUtils {

    private Context context;
    private SharedPreferences sharedPreferencesEncryption;
    private SharedPreferences sharedPreferencesAuth;

    private String publicKey;
    private PrivateKey privateKey;

    public CryptoUtils(Context context, SharedPreferences sharedPreferencesAuth) {
        this.context = context;
        this.sharedPreferencesEncryption = context.getSharedPreferences(Config.SHARED_PREF_ENCRYPTION_VARS, Context.MODE_PRIVATE);
        this.sharedPreferencesAuth = sharedPreferencesAuth;
    }

    /**
     * Encrypt a file using AES.
     * @param input the input file.
     * @param output where to store the output after encryption.
     * @param bucketId the bucket id where the file stored in [used for creating encryption key]
     * @param fileId the files id [used for creating encryption key]
     * @throws Exception problem encrypting file.
     */
    public void encryptFile(File input, File output, String bucketId, String fileId) throws Exception {
        new AESFiles().encrypt(input, output, getEncryptionValues(bucketId, fileId));
    }

    /**
     * Decrypt a file using AES.
     * @param input the encrypted input file.
     * @param output where to store the output after decryption.
     * @param bucketId the bucket id where the file stored in [used for creating encryption key]
     * @param fileId the files id [used for creating encryption key]
     * @throws Exception problem decrypting file.
     */
    public void decryptFile(File input, File output, String bucketId, String fileId) throws Exception {
        new AESFiles().decrypt(input, output, getEncryptionValues(bucketId, fileId));
    }

    /**
     * Take an input array of bytes and encode it as the storj core does.
     * This firstly sha256es encrypts the file, then hex encodes it.
     * Then, it RIPEMD160es the result and again hex encodes it, before returning.
     *
     * this algorithm is used for file encryption
     *
     * @param input a byte array to encrypt.
     * @return the byte[] after encrypting.
     */
    public static byte[] rmd160Sha256(byte[] input){
        byte[] sha256 = Hashing.sha256().hashBytes(input).asBytes();
        return Hex.encode(rmd160(Hex.encode(sha256)));
    }

    /**
     * Take an input array of bytes and encode it as the storj core does.
     * This firstly sha56es encrypts the file and then RIPEMD160es the result
     *
     * this algorithm is used for the file id generation
     *
     * @param input
     * @return
     */
    public static byte[] rmd160Sha256_noHexEncode(byte[] input){
        byte[] sha256 = Hashing.sha256().hashBytes(input).asBytes();
        return rmd160(sha256);
    }

    /**
     * RIPEMD 160 hashing algorithm
     *
      * @param input
     * @return
     */
    public static byte[] rmd160(byte[] input){
        RIPEMD160Digest digest = new RIPEMD160Digest();
        digest.update(input, 0, input.length);
        byte[] output = new byte[digest.getDigestSize()];
        digest.doFinal (output, 0);
        return output;
    }

    /**
     * A more efficient version of rmd160sha256 for files, as it uses streaming.
     * @param file the file to hash.
     * @return the file after sha256 and ripemd160.
     * @throws Exception
     */
    public static byte[] getRipemdSha256File(File file) throws Exception{
        // Get sha256 for the file.
        byte[] sha256sBytes = com.google.common.io.Files.hash(file, Hashing.sha256()).asBytes();

        // Updated in v3.0 of storj bridge.
        //sha256sBytes = Hex.encode(sha256sBytes);

        // Get RIPEMD160 for that.
        RIPEMD160Digest digest = new RIPEMD160Digest();
        digest.update (sha256sBytes, 0, sha256sBytes.length);
        byte[] output = new byte[digest.getDigestSize()];
        digest.doFinal (output, 0);
        output = Hex.encode(output);
        return output;
    }

    public static String calculateFileId(String bucketId, String fileName){
        String concatenated = bucketId + fileName;
        return bytesToHex(rmd160Sha256_noHexEncode(concatenated.getBytes())).substring(0, 24);
    }

    public static KeyPair generateKeyPair() {

        try {
            //using spongycastle provider
            Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);

            // there are different curves in ECDSA. Storj signatures use the curve called secp256k1
            ECGenParameterSpec ecGenSpec = new ECGenParameterSpec("secp256k1");

            KeyPairGenerator g = null;
            g = KeyPairGenerator.getInstance("ECDSA", "SC");
            g.initialize(ecGenSpec, new SecureRandom());

            return g.generateKeyPair();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }

        return null;
    }

    // from http://stackoverflow.com/questions/9655181/how-to-convert-a-byte-array-to-a-hex-string-in-java
    final protected static char[] hexArray = "0123456789abcdef".toCharArray();
    public static String bytesToHex(byte[] bytes) {

        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    // from http://stackoverflow.com/questions/140131/convert-a-string-representation-of-a-hex-dump-to-a-byte-array-using-java
    public static byte[] hexToBytes(String hex) {
        int len = hex.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4)
                    + Character.digit(hex.charAt(i+1), 16));
        }
        return data;

    }

    /**
     * Generates a keypair and stores the private key and the public key to the SharedPreferences
     */
    public void initializeSignatureBasedAuthentication(){
        KeyPair keyPair = CryptoUtils.generateKeyPair();

        storePrivatKey(keyPair.getPrivate());
        storePublicKeyInBridgeFormat(keyPair.getPublic());
    }

    /**
     * load private key from shared preferences
     *
     * @return PrivateKey
     */
    public PrivateKey loadPrivatKey(){

        if(privateKey == null) {

            String privateKeyEncodedBase64 = sharedPreferencesAuth.getString("authenticationPrivateKey", null);
            byte[] privateKeyEncoded = Base64.decode(privateKeyEncodedBase64, Base64.DEFAULT);

            //using spongycastle provider
            Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);

            try {
                PKCS8EncodedKeySpec pubKeySpec = new PKCS8EncodedKeySpec(privateKeyEncoded);
                KeyFactory keyFactory = KeyFactory.getInstance("ECDSA");
                privateKey = keyFactory.generatePrivate(pubKeySpec);
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        return privateKey;
    }

    /**
     * stores private key to shared preferences
     *
     * @return PrivateKey
     */
    public void storePrivatKey(PrivateKey privateKey){

        this.privateKey = privateKey;

        SharedPreferences.Editor spEditor = sharedPreferencesAuth.edit();

        byte[] privateKeyEncoded = privateKey.getEncoded();
        String privateKeyEncodedBase64 = Base64.encodeToString(privateKeyEncoded, Base64.DEFAULT);
        spEditor.putString("authenticationPrivateKey", privateKeyEncodedBase64);

        spEditor.commit();
    }

    /**
     * load public key from shared preferences
     *
     * @return PrivateKey
     */
    public String loadPublicKey(){
        if(publicKey == null || publicKey.equals("")){
            publicKey = sharedPreferencesAuth.getString("authenticationPublicKey", null);
        }

        return publicKey;
    }

    /**
     * stores public key to shared preferences
     *
     * @return PrivateKey
     */
    public void storePublicKey(String publicKeyForBridge){

        publicKey = publicKeyForBridge;

        SharedPreferences.Editor spEditor = sharedPreferencesAuth.edit();
        spEditor.putString("authenticationPublicKey", publicKeyForBridge);
        spEditor.commit();
    }

    /**
     * transforms the Public Key to the form that is readable for the bridge (String)
     * @param publicKey
     * @return String public key for bridge
     */
    public static String transformPublicKeyToStringForBridge(PublicKey publicKey){
        BCECPublicKey publicKeySC = (BCECPublicKey) publicKey;
        return CryptoUtils.bytesToHex(publicKeySC.getQ().getEncoded(true));
    }

    /**
     * trensforms the Public Key to the form that is readable for the bridge (String)
     * @param publicKey
     * @return String public key for bridge
     */
    public void storePublicKeyInBridgeFormat(PublicKey publicKey){
        storePublicKey(CryptoUtils.transformPublicKeyToStringForBridge(publicKey));
    }

    /**
     * removes both public and private key from shared preferences
     */
    public void resetAuthenticationKeys(){
        SharedPreferences.Editor spEditor = sharedPreferencesAuth.edit();
        spEditor.remove("authenticationPrivateKey");
        spEditor.remove("authenticationPublicKey");
        spEditor.commit();
    }

    public EncryptionValues getEncryptionValues(String bucketId, String fileId){

        // if the bucket key exists in the shared preferences, then load it from there, if not calculate it and store it in the shared preferences
        if(sharedPreferencesEncryption.contains(bucketId)) {
            return generateEncryptionValues(CryptoUtils.hexToBytes(sharedPreferencesEncryption.getString(bucketId, null)), fileId);
        }else{
            SeedUtils seedUtils = new SeedUtils(context);
            byte[] bucketKey = generateBucketKey(seedUtils.getSeed(), bucketId);

            // store hexadecimal representation of bucket key in shared preferences
            SharedPreferences.Editor spEditor = sharedPreferencesEncryption.edit();
            spEditor.putString(bucketId, CryptoUtils.bytesToHex(bucketKey));
            spEditor.commit();

            return generateEncryptionValues(bucketKey, fileId);
        }
    }

    public static EncryptionValues generateEncryptionValues(byte[] bucketKey, String fileId) {

        byte[] fileId_binary = CryptoUtils.hexToBytes(fileId);

        // concatenate the bucket key and the fileId-bytes
        byte[] fileKeySourceData = ArrayUtils.addAll(bucketKey, fileId_binary);

        // hash the data using SHA-512. The first 32 bytes (256 bits) are the file key
        byte[] fileKey = Arrays.copyOfRange(Hashing.sha512().hashBytes(fileKeySourceData).asBytes(), 0, 32);

        // generate encryption key using sha-256
        // CAUTION: this procedure does NOT hash the fileKey, but instead its hexadecimal representation as a UTF-8 string.
        byte[] encryptionKey = Hashing.sha256().hashString(CryptoUtils.bytesToHex(fileKey), Charset.defaultCharset()).asBytes();

        // generate initialization vector by hashing the fileId using the algorithm RIPMED-160
        // CAUTION: this procedure does NOT hash the binaries of the fileId, but instead its hexadecimal representation as a UTF-8 string.
        // the first 16 bytes (128 bits) are the initialization vector
        byte[] initializationVector = Arrays.copyOfRange(CryptoUtils.rmd160(fileId.getBytes()), 0, 16);

        return new EncryptionValues(encryptionKey, initializationVector);

    }


    public static byte[] generateBucketKey(byte[] seed, String bucketId){

        byte[] bucketId_binary = CryptoUtils.hexToBytes(bucketId);

        // concatenate the seed and bucketId-bytes.
        byte[] bucketKeySourceData = ArrayUtils.addAll(seed, bucketId_binary);

        // hash the data using SHA-512. The first 32 bytes (256 bits) are the bucket key
        byte[] bucketKey = Arrays.copyOfRange(Hashing.sha512().hashBytes(bucketKeySourceData).asBytes(), 0, 32);

        return bucketKey;
    }
}
