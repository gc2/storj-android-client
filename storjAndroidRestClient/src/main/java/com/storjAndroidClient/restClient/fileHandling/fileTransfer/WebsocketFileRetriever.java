package com.storjAndroidClient.restClient.fileHandling.fileTransfer;

import com.google.gson.Gson;
import com.storjAndroidClient.restClient.model.AuthorizationModel;
import com.storjAndroidClient.restClient.model.ShardPointer;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Logger;

/**
 * Created by Gabriel Comte on 14.10.2016.
 *
 * NOT USED ANYMORE!
 * Replaced by HttpShardTransfer
 */

public class WebsocketFileRetriever extends WebSocketClient {

    private Logger logger = Logger.getLogger(this.getClass().getName());
    private Gson gson = new Gson();
    private ShardPointer shardPointer;
    private File outputFile;
    private AuthorizationModel authModel;
    private FileChannel channel;
    private CountDownLatch latch;

    public WebsocketFileRetriever(URI serverURI, ShardPointer shardPointer, File outputFile, CountDownLatch latch){
        super(serverURI, new Draft_17(), null, 99999);
        this.shardPointer = shardPointer;
        this.outputFile = outputFile;
        authModel = new AuthorizationModel();
        authModel.setToken(shardPointer.getToken());
        authModel.setOperation(shardPointer.getOperation());
        authModel.setHash(shardPointer.getHash());
        this.latch = latch;
    }

    public void onOpen(ServerHandshake serverHandshake) {
        logger.info("Connected to farmer.");
        ByteBuffer buffer = ByteBuffer.allocate(gson.toJson(authModel).getBytes().length);
        buffer.put(gson.toJson(authModel).getBytes());
        send(gson.toJson(authModel));

        try {
            outputFile.getParentFile().mkdirs();
            outputFile.createNewFile();
            channel = new FileOutputStream(outputFile, true).getChannel();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMessage(String s){
        logger.info("Received text... " + s);
    }

    @Override
    public void onMessage(ByteBuffer b){
        logger.info("Received binary... " + b);
        try {
            channel.write(b);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        logger.info("Closing connection. " + i + s + b);
        try {
            channel.close();
            latch.countDown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onError(Exception e) {
        e.printStackTrace();
    }
}
