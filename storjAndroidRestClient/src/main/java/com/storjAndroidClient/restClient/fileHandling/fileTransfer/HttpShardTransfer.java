package com.storjAndroidClient.restClient.fileHandling.fileTransfer;

import android.util.Log;

import com.storjAndroidClient.restClient.backgroundServices.templateClasses.ShardStatus;
import com.storjAndroidClient.restClient.helperClasses.FileSizeUtils;
import com.storjAndroidClient.restClient.helperClasses.JavaNioFiles;
import com.storjAndroidClient.restClient.model.Contact;
import com.storjAndroidClient.restClient.model.ShardPointer;
import com.storjAndroidClient.restClient.model.ShardUploadParameters;
import com.storjAndroidClient.restClient.model.UploadableShard;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

/**
 * Created by Gabriel Comte on 9.2.2017.
 */

public class HttpShardTransfer {

    private static final int BUFFER_SIZE = 1024 * 32; // using 32K buffer, as supposed by http://stackoverflow.com/questions/10143731/android-optimal-buffer-size in 2012

    public static void sendShard(UploadableShard uploadableShard) throws Exception  {
        HttpURLConnection httpURLConnection = null;

        ShardUploadParameters params = uploadableShard.getUploadParameters();

        try {
            httpURLConnection = setUpConnection(ShardHTTPTransferRequestMethod.POST, params.getFarmer(), params.getHash(), params.getToken(), uploadableShard.getSize());

            //Send request
            OutputStream os = httpURLConnection.getOutputStream();
            DataOutputStream wr = new DataOutputStream (os);

            // send uploadableShard data as binary - note this will need changing to read in small amounts to save memory.. when it's working.
            wr.write(JavaNioFiles.readAllBytes(uploadableShard.getPath()));

            wr.flush();
            wr.close();

            //Get Response
            if(httpURLConnection.getResponseCode() != HttpURLConnection.HTTP_OK){
                throw new Exception("UploadableShard could not be sent. HTTP-Code: " + httpURLConnection.getResponseCode());
            }

            InputStream is = httpURLConnection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;

            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            Log.d("UPLOADING SHARD", "[" + uploadableShard.getPath() + " - " + uploadableShard.getHash() + "] received response: " + response);

            uploadableShard.setTransferStatus(ShardStatus.TRANSFERRED);

        } catch (IOException e) {
            uploadableShard.setTransferStatus(ShardStatus.FAILED);
            throw e;
        }  finally {
            if(httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    public static void receiveShard(ShardPointer shardPointer) throws Exception {
        HttpURLConnection httpURLConnection = null;
        try {

            httpURLConnection = setUpConnection(ShardHTTPTransferRequestMethod.GET, shardPointer.getFarmer(), shardPointer.getHash(), shardPointer.getToken(), shardPointer.getSize());

            httpURLConnection.setDefaultUseCaches(false);
            httpURLConnection.connect();

            //Get Response
            if(httpURLConnection.getResponseCode() != HttpURLConnection.HTTP_OK){
                throw new Exception("UploadableShard could not be received. HTTP-Code: " + httpURLConnection.getResponseCode());
            }

            // opens input stream for reading from the HTTP connection
            InputStream inputStream = httpURLConnection.getInputStream();

            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(shardPointer.getPath());


            int bytesRead = -1;

            Calendar beforeAction = Calendar.getInstance();
            long timestampBeforeAction = beforeAction.getTimeInMillis();


            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            Calendar afterAction = Calendar.getInstance();
            long timestampAfterAction = afterAction.getTimeInMillis();

            outputStream.close();

            Log.d("DOWNLOADING SHARD", "[" + shardPointer.getPath() + "] size: " + FileSizeUtils.humanReadableByteCount(shardPointer.getSize(), true) + ", time: " + (timestampAfterAction - timestampBeforeAction) / 1000 + " seconds, buffer-size: " + FileSizeUtils.humanReadableByteCount(BUFFER_SIZE, false));

            shardPointer.setTransferStatus(ShardStatus.TRANSFERRED);

        } catch (Exception e) {

            shardPointer.setTransferStatus(ShardStatus.FAILED);
            e.printStackTrace();

        } finally {
            if(httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    protected static HttpURLConnection setUpConnection(ShardHTTPTransferRequestMethod method, Contact farmer, String hash, String token, long shardsize) throws IOException {
        URL farmerUrl = new URL("http://" + farmer.getAddress() + ":" + farmer.getPort() + "/shards/" + hash + "?token=" + token );

        HttpURLConnection httpURLConnection = (HttpURLConnection) farmerUrl.openConnection();

        httpURLConnection.setRequestMethod(method.toString()); // "POST" for uploading | "GET" for downloading

        // all these settings are copied from https://github.com/nholloh/storj.net/blob/master/Storj.net/Storj.net/Util/HttpShardTransferUtil.cs
        httpURLConnection.setFixedLengthStreamingMode(shardsize);
        httpURLConnection.addRequestProperty("x-storj-node-id", farmer.getNodeID());
        httpURLConnection.setRequestProperty("Content-Type", "application/octet-stream");
        httpURLConnection.setConnectTimeout(240000);

        if(method == ShardHTTPTransferRequestMethod.POST) {
            httpURLConnection.setDoOutput(true);
        }else{
            httpURLConnection.setDoOutput(false);
        }

        httpURLConnection.setDoInput(true);

        return httpURLConnection;
    }
}
