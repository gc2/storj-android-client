package com.storjAndroidClient.restClient.fileHandling.fileTransfer;

import com.google.gson.Gson;
import com.storjAndroidClient.restClient.helperClasses.JavaNioFiles;
import com.storjAndroidClient.restClient.model.ShardUploadParameters;
import com.storjAndroidClient.restClient.model.AuthorizationModel;
import com.storjAndroidClient.restClient.model.UploadableShard;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

import java.io.File;
import java.net.URI;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Logger;

/**
 * Created by Gabriel Comte on 24.10.2016.
 *
 * NOT USED ANYMORE!
 * Replaced by HttpShardTransfer
 */

public class WebSocketShardSender extends WebSocketClient {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private Gson gson = new Gson();
    private UploadableShard shard;
    private ShardUploadParameters destination;
    private AuthorizationModel authModel;
    private CountDownLatch latch;

    public WebSocketShardSender(URI serverURI, UploadableShard uploadableShard, ShardUploadParameters destination, CountDownLatch latch){
        super(serverURI, new Draft_17(), null, 99999);
        this.shard = uploadableShard;
        this.destination = destination;
        this.latch = latch;

        System.out.println("destination.getToken(): " + destination.getToken());
        System.out.println("destination.getOperation(): " + destination.getOperation());
        System.out.println("destination.getHash(): " + destination.getHash());

        authModel = new AuthorizationModel();
        authModel.setToken(destination.getToken());
        authModel.setOperation(destination.getOperation());
        authModel.setHash(destination.getHash());

    }

    public void onOpen(ServerHandshake serverHandshake) {
        System.out.println("YYY 1");
        File shardFile = new File(shard.getPath());
        System.out.println("YYY 2");
        // send auth as text.
        send(gson.toJson(authModel));
        System.out.println("YYY 3");
        // send shard data as binary - note this will need changing to read in small amounts to save memory.. when it's working.
        send(JavaNioFiles.readAllBytes(shardFile.getPath()));
        System.out.println("YYY 4");
    }

    public void onMessage(String s) {
        logger.info(s);
    }

    public void onClose(int i, String s, boolean b) {
        logger.info("Websocket closed with reason" + i + s + b);
        latch.countDown();
    }

    public void onError(Exception e) {
        e.printStackTrace();
    }
}
