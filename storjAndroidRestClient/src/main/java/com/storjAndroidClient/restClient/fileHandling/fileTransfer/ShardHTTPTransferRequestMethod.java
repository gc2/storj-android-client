package com.storjAndroidClient.restClient.fileHandling.fileTransfer;

/**
 * Created by Gabriel Comte on 09.02.2016.
 */

public enum ShardHTTPTransferRequestMethod {
    POST, GET;
}
