package com.storjAndroidClient.restClient.fileHandling.encryption;

import com.storjAndroidClient.restClient.model.EncryptionValues;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
/**
 * Created by Gabriel Comte on 14.10.2016.
 */

public class AESFiles {
    private byte[] getKeyBytes(final byte[] key) throws Exception {
        byte[] keyBytes = new byte[16];
        System.arraycopy(key, 0, keyBytes, 0, Math.min(key.length, keyBytes.length));
        return keyBytes;
    }

    private Cipher getCipherEncrypt(EncryptionValues ev) throws Exception {
        return getCipher(Cipher.ENCRYPT_MODE, ev);
    }

    private Cipher getCipherDecrypt(EncryptionValues ev) throws Exception {
        return getCipher(Cipher.DECRYPT_MODE, ev);
    }

    private Cipher getCipher(int encryptionMode, EncryptionValues ev) throws InvalidAlgorithmParameterException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
        SecretKeySpec secretKeySpec = new SecretKeySpec(ev.getEncryptionKey(), "AES");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(ev.getInitializationVector());
        cipher.init(encryptionMode, secretKeySpec, ivParameterSpec);
        return cipher;
    }

    public void encrypt(File inputFile, File outputFile, EncryptionValues ev) throws Exception {
        Cipher cipher = getCipherEncrypt(ev);
        FileOutputStream fos = null;
        CipherOutputStream cos = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(inputFile);
            fos = new FileOutputStream(outputFile);
            cos = new CipherOutputStream(fos, cipher);
            byte[] data = new byte[1024];
            int read = fis.read(data);
            while (read != -1) {
                cos.write(data, 0, read);
                read = fis.read(data);
            }
            cos.flush();
        } finally {
            if (cos != null) {
                cos.close();
            }
            if (fos != null) {
                fos.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
    }

    public void decrypt(File inputFile, File outputFile, EncryptionValues ev) throws Exception {

        Cipher cipher = getCipherDecrypt(ev);

        System.out.println("cipher: " + cipher);
        FileOutputStream fos = null;
        CipherInputStream cis = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(inputFile);
            cis = new CipherInputStream(fis, cipher);
            fos = new FileOutputStream(outputFile);
            System.out.println(inputFile.getAbsolutePath());
            System.out.println(outputFile.getAbsolutePath());
            byte[] data = new byte[1024];
            System.out.println("data: " + data);
            int read = cis.read(data);
            while (read != -1) {
                fos.write(data, 0, read);
                read = cis.read(data);
            }
        } finally {
            fos.close();
            cis.close();
            fis.close();
        }
    }
}
