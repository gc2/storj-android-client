package com.storjAndroidClient.restClient.fileHandling.encryption;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.storjAndroidClient.restClient.Config;

import org.bitcoinj.crypto.MnemonicCode;
import org.bitcoinj.crypto.MnemonicException;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Gabriel Comte on 28.2.2017.
 */

public class SeedUtils {

    private static final String MNEMONIC_KEY = "mnemonic";
    private static final String SEED_KEY = "seed";

    SharedPreferences sharedPreferencesEncryption;

    public SeedUtils(Context context) {
        this.sharedPreferencesEncryption = context.getSharedPreferences(Config.SHARED_PREF_ENCRYPTION_VARS, Context.MODE_PRIVATE);
    }

    /**
     * imports the mnemonic stores it, as well as the derived seed
     * if there is already an entry, the new mnemonic is only imported if overwrite is set to true
     *
     * @param mnemonic
     * @param overwrite
     */
    public void importMnemonic(String mnemonic, boolean overwrite) throws MnemonicException {

        // throw MnemonicException if mnemonic is not valid
        MnemonicCode.INSTANCE.check(new ArrayList<String>(Arrays.asList(mnemonic.split("\\s+"))));

        storeMnemonic(mnemonic, overwrite);
        storeSeed(mnemonic, overwrite);
    }

    /**
     * exports the mnemonic
     */
    public String exportMnemonic(){
        return sharedPreferencesEncryption.getString(MNEMONIC_KEY, null);
    }

    /**
     * returns true if there is an entry inside the shared references for "mnemonic" as well as an entry for "seed"
     */
    public boolean seedInformationIsStored(){
        return sharedPreferencesEncryption.contains(MNEMONIC_KEY) && sharedPreferencesEncryption.contains(SEED_KEY);
    }

    /**
     * derives the 512 bit seed from the mnemonic
     * and stores a hexadecimal representation of it to the shared preferences
     * if seed is already set, the new seed is only stored if overwrite is set to true
     *
     * @param mnemonic
     * @param overwrite
     */
    private void storeSeed(String mnemonic, boolean overwrite){
        if(sharedPreferencesEncryption.contains(SEED_KEY) && !overwrite){
            Log.d("Seed", "Seed is not being stored, as there is already a seed stored in the shared preferences");
        }else{
            storeSeed(mnemonic);
        }
    }

    /**
     * derives the 512 bit seed from the mnemonic
     * and stores a hexadecimal representation of it to the shared preferences
     *
     * @param mnemonic
     */
    private void storeSeed(String mnemonic){

        // mnemonic as a list
        ArrayList<String> mySeedList = new ArrayList<String>(Arrays.asList(mnemonic.split("\\s+")));

        SharedPreferences.Editor spEditor = sharedPreferencesEncryption.edit();
        spEditor.putString(SEED_KEY, CryptoUtils.bytesToHex(MnemonicCode.toSeed(mySeedList, "")));
        spEditor.commit();
    }

    /**
     * reads the seed out of the shared preferences and returns it as a byte array
     *
     * @return the 512 bit seed as a byte array
     */
    public byte[] getSeed(){
        return CryptoUtils.hexToBytes(sharedPreferencesEncryption.getString(SEED_KEY, null));
    }

    /**
     * stores the mnemonic to the shared preferences
     * if the mnemonic is already set, the new mnemonic is only stored if overwrite is set to true
     *
     * @param mnemonic
     * @param overwrite
     */
    private void storeMnemonic(String mnemonic, boolean overwrite){
        if(sharedPreferencesEncryption.contains(MNEMONIC_KEY) && !overwrite){
            Log.d("Mnemonic", "Mnemonic is not being stored, as there is already a mnemonic stored in the shared preferences");
        }else{
            storeMnemonic(mnemonic);
        }
    }

    /**
     * stores the mnemonic to the shared preferences
     *
     * @param mnemonic
     */
    private void storeMnemonic(String mnemonic){
        SharedPreferences.Editor spEditor = sharedPreferencesEncryption.edit();
        spEditor.putString(MNEMONIC_KEY, mnemonic);
        spEditor.commit();
    }

    /**
     * checks whether a word exists in the word list or not
     *
     * @param wordToBeChecked
     * @return true if word is in wordlist | false if it is not
     */
    public static boolean isWordValid(String wordToBeChecked){
        for(String wordFromList : MnemonicCode.INSTANCE.getWordList()){
            if(wordToBeChecked.equals(wordFromList)){
                return true;
            }
        }
        return false;
    }
}
