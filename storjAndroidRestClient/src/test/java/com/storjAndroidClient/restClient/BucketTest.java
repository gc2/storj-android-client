package com.storjAndroidClient.restClient;

import com.storjAndroidClient.restClient.model.Bucket;
import com.storjAndroidClient.restClient.model.BucketStatus;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by Gabriel Comte on 06 .10.2016.
 */

@RunWith(RobolectricTestRunner.class)
public class BucketTest {
    @Test
    public void Bucket_checkStatusConvertion() throws Exception {

        // settting test variables
        String statusActive = "Active";
        String statusInactive = "Inactive";

        // Set status as String, get status as statuscode
        Bucket testBucket = new Bucket();
        testBucket.setStatus(statusActive);
        assertEquals(BucketStatus.ACTIVE, testBucket.getStatusKey());

        // Set status as String, get status as statuscode
        // but this time, using constants (static final variables)
        testBucket = new Bucket();
        testBucket.setStatusByKey(BucketStatus.INACTIVE);
        assertEquals(BucketStatus.INACTIVE, testBucket.getStatusKey());

        // Set status as statuscode, get status as String
        testBucket = new Bucket();
        testBucket.setStatusByKey(BucketStatus.INACTIVE);
        assertEquals(statusInactive, testBucket.getStatus());

        // Set status as statuscode, get status as String
        // but this time, using constants (static final variables)
        testBucket = new Bucket();
        testBucket.setStatusByKey(BucketStatus.ACTIVE);
        assertEquals(Bucket.STATUS_ACTIVE_KEYWORD, testBucket.getStatus());
    }

    @Test
    public void Bucket_checkCreatedDateTimeConvertion() throws Exception {

        // settting test variables
        String createdIso8601 = "2016-07-10T11:45:18.125Z";
        long createdUnixTimeStamp = 1468151118125L; // milliseconds since January 1st, 1970
        DateTimeZone.setDefault(DateTimeZone.UTC);
        DateTime createdDateTime = new DateTime(createdUnixTimeStamp);

        // Set created as String, get created as DateTime
        Bucket testBucket = new Bucket();
        testBucket.setCreated(createdIso8601);
        assertEquals(createdDateTime, testBucket.getCreatedAsDateTime());

        // Set created as DateTime, get created as String
        testBucket = new Bucket();
        testBucket.setCreatedAsDateTime(createdDateTime);
        assertEquals(createdIso8601, testBucket.getCreated());
    }
}
